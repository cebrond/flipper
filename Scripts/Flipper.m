function [A,B,C,L,compteur,valid,cond1,cond2,eq1,eq2,eq3,eps,beta,theta2D,phi2D,NormCenter,varargout]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,choice,varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                  _.-,
%              .--'  '-._
%           _/`-  _      '.
%          '----'._`.----. \
%                   `     \;
%                         ;_\
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Name: FLIPPER (FLows at ImPosed Precession within Ellipsoids in Rotation)
% Author: D. CEBRON  (ISTerre, Grenoble, France)
% Date: Frebruary, 20, 2014
% ----------------------------------------------
% Goal: gives the bulk uniform vorticity flows in precessing ellipsoids
% Main ouptut : bulk flow vorticity
% Ref: based on Noir et al. 2003, Cebron et al. 2010, Noir&Cebron 2013
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%--------------------------------------------------------------------------
%---- INPUT arguments : How To --------------------------------------------
%--------------------------------------------------------------------------
% choice: choose the model used for calculations
% ---- Steady flows (algebraic solving) : 0<choice<5 ----------------------
%-1 -> ELLIPSOID: Poincare (inviscid) flow based on Cebron et al. 2010
% 0 -> SPHEROID : reduced model (linear viscous term) with lr, li
% 1 -> SPHEROID : Busse 1968 based on the formulation of Noir et al. 2003
% 2 -> ELLIPSOID: Triaxial ellipsoids of Cebron et al. 2010
% 3 -> SPHEROID : General model with no Spin-up condition, Noir&Cebron 2013
% 4 -> SPHEROID : General model with Lsup, Noir&Cebron 2013
% 5 -> ELLIPSOID: General model for Cebron et al. 2010 with no Spin-up eq.
% 6 -> ELLIPSOID: General model with Lsup for Cebron et al. 2010
% ---- Unsteady flows (ODEs solving) : 7<choice<12 ------------------------
% 7 -> ELLIPSOID: General model with Lsup, Noir&Cebron 2013
% 8 -> ELLIPSOID: General model with no Spin-up condition, Noir&Cebron 2013
% 9 -> ELLIPSOID: Reduced model (linear viscous term) with lr, li
% 10-> ELLIPSOID: General model for Cebron et al. 2010 with no Spin-up eq.
% 11-> ELLIPSOID: General model with Lsup for Cebron et al. 2010
% 12-> ELLIPSOID: General model for Cebron et al. 2010 with reduced torque
%--------------------------------------------------------------------------
% (a,b,c): main ellipsoid axes lengths (along Ox, Oy Oz)
% Ek: Ekman number considered (based on the length R=(abc)^(1/3))
% (Wpx,Wpy,Wpz): precession vector in the precessing frame (norm=Po)
% details (optional): details in the terminal (1:shown / default:not shown)
% L=[lr li lsup] (optional): damping factor of the spinover
%           (typically, lr=-2.62, li=0.258, lsup=0, see Noir & Cebron 2013)
% Lauto=0: the provided input L is used (of L<>0, L is changed, see below)
% Lauto=1: L based on Zhang et al. 2004, Noir&Cebron2013 for Ek<<1
% Lauto=2: adds to Lauto=1 the viscous corrections (Noir et al. 2001
% for lr and Hollerbach&Kerswell 1995 for li), default L
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
%---- OUTPUT of the script ------------------------------------------------
%--------------------------------------------------------------------------
% (A,B,C): Fluid rotation rates in the precession frame
% L: L used for calculations
% compteur: number of solutions of the implicit eq. in C
% eq1,eq2,eq3 : residual of equations when solutions are plugged into them
% varargout : time vector when ODEs are solved (6<choice<10)
%--------------------------------------------------------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- SCRIPT --------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
details=0; Lauto=2; SupArg=[]; rICB=0; Els=0; Bmag=[0 0 0];

if(length(varargin)==1), details=varargin{1}; Lauto=2; SupArg=[details];
elseif(length(varargin)>1), details=varargin{1}; Lauto=varargin{2};
    if((length(varargin)==2)&&(Lauto==0))
        display('Lauto=0 but no damping factors L is supplied');
        display('Lauto changed to'); Lauto=2
    end
    SupArg=[details,Lauto];
    if(length(varargin)>=3),
        L=varargin{3};
        if(~(Lauto==0)), display('Lauto<>0 : the input L is not used'); end
        SupArg=[details,Lauto,L];
    end
end

if(length(varargin)>=4),
    rICB=varargin{4}; SupArg=[details,Lauto,L,rICB];
end

if(length(varargin)>=5),
    Els=varargin{5}; SupArg=[details,Lauto,L,rICB,Els];
end
if(length(varargin)>=6),
    Bmag=varargin{6}; SupArg=[details,Lauto,L,rICB,Els,Bmag];
end

if(details==1),display('If Wp<>0, solution (0,0,0) is not considered'); end
A=[]; B=[]; C=[];
%---- Obvious solution (0,0,0) when Wp=0 ----------------------------------
if(sqrt(Wpx^2+Wpy^2)==0),
    A=0; B=0; C=1; compteur=1; eq1=0; eq2=0; eq3=0; return;
end
if(Wpx==0),Wpx=1e-20;end;if(Wpy==0),Wpy=1e-20;end;if(Wpz==0),Wpz=1e-20;end;
%--------------------------------------------------------------------------
%---- L calculation if Lauto=1 or 2 ---------------------------------------
if(Lauto>0)
    if(imag(Ek)==0)
        if(Lauto==1)
            L=LSO(a,b,c,choice);
        elseif(Lauto==2)
            L=LSO(a,b,c,choice,Ek);
        end
    else
        if(Lauto==1)
            L0=LSO(a,b,c,choice);
        elseif(Lauto==2)
            L0=LSO(a,b,c,choice,abs(Ek));
        end
        
        if((choice==7)||(choice==11))
            L=[-0.0088 0 L0(3)];
            %         elseif((choice==8)||(choice==9)||(choice==10)||(choice==12))
        else
            L=[-0.0088 0 0];
        end
    end
elseif(Lauto==0)
    if(L(1)>=0),display('WARNING! No-sens positive viscous factor Lr!');end
    if((L(3)>=0)&&((choice==4)||(choice==7)||(choice==11)))
        display('WARNING! No-sens positive spin-up factor Lsup!');
    end
end
if(~(abs(rICB)==0))
    if(abs(imag(rICB))==0)
        L=L.*(1+abs(rICB).^4)./(1-abs(rICB).^5);
    elseif(abs(real(rICB))==0)
        L=L.*1./(1-abs(rICB).^5);
    else
        L=L.*abs(rICB).^4./(1-abs(rICB).^5);
    end
end
%--------------------------------------------------------------------------
%---- Useful quantities for calculations ----------------------------------
eta=1-c/a; epsil=(c^2-a^2)/(c^2+a^2);
if(details==1), display(eta); end
Wp=sign(Wpz)*sqrt(Wpx.^2+Wpy.^2+Wpz^2);
alpha=asin(sqrt(Wpx.^2+Wpy.^2)./Wp);
phi=atan2(Wpy,Wpx);     % azimut angle in xOy of precession vector Wp
co=cos(alpha); si=sin(alpha);
%--------------------------------------------------------------------------
%---- Busse (1968) validity limits ----------------------------------------
cond1=0; cond2=0; valid=1;
if(choice>-1)
    cond1=min(min(abs(Wp*sin(alpha)/sqrt(Ek)),abs(alpha/abs(Wpz+eta))),abs(Wp/eta));
    etA=1-b/a;
    cond2=min(min(abs(Wp*sin(alpha)/sqrt(Ek)),abs(alpha/abs(Wpz+etA))),abs(Wp/etA));
    criterion=0.1;
    if((cond1>criterion)||(cond2>criterion))
        if(cond1>criterion)
            valid=0;
            if(details==1)
                display('CHECK Busse (1968) validity limits!!');
                display('Polar ellipticity condition violated: see just below');
                display(cond1);
                if(cond1==abs(Wp*sin(alpha)/sqrt(Ek))), display('min=Wp*sin(alpha)/sqrt(E)');
                elseif(cond1==abs(alpha/abs(Wpz+eta))), display('min=alpha/(W.k+eta)');
                elseif(cond1==abs(Wp/eta)), display('min=Wp/eta');
                end
            end
        end
        if((cond2>criterion)&&(~(a==b)))
            valid=0;
            if(details==1)
                display('CHECK Busse (1968) validity limits!!');
                display('Equatorial ellipticity condition violated: see just below');
                display(cond2);
                if(cond2==abs(Wp*sin(alpha)/sqrt(Ek))), display('min=Wp*sin(alpha)/sqrt(E)');
                elseif(cond2==abs(alpha/abs(Wpz+etA))), display('min=alpha/(W.k+eta)');
                elseif(cond2==abs(Wp/etA)), display('min=Wp/eta');
                end
            end
        end
    end
end
%--------------------------------------------------------------------------

if(choice==-1) % Poincare flow (Cebron et al. 2010)
    C=Ek; % Arbitrary (adhoc viscous in sphere => C=cos(alpha)^2)
    if(C<0.1), display('n=-1 (Poincare inviscid flow)=> input Ek defines the fluid axial rotation rate, usually fixed to 1'); end;
    beta12=(a^2+b^2)/(2*a*b);
    gam1=C/beta12*(a^2-c^2)+2*Wpz*a*b; gam2=C/beta12*(b^2-c^2)+2*Wpz*a*b;
    A=C/beta12*(c^2+b^2)/gam1*Wpx; B=C/beta12*(c^2+a^2)/gam2*Wpy;
    compteur=1; eq1=0; eq2=0; eq3=0;
    
elseif(choice<7) % Distinction between ODE solving and algebraic solving
    flag=0;
    while(flag==0)  % Loop on Wpz if convergence difficulty are encountered
        compteur=0; % count valid solutions of implicit eq. in C
        
        if((choice==0)||(choice==1)) % Busse 68
            if(details==1), display('Busse 1968'); end;
            if(choice==0)
                %                 Nroots=3;
                polynom=[epsil^2/Wp/si/(1+epsil) -epsil*(2*co*Wp+2*L(2)*sqrt(Ek)+epsil-2*epsil*co*Wp)/Wp/si/(1+epsil) (2*epsil*L(2)*sqrt(Ek)-2*epsil^2*co*Wp+Wp^2*si^2*epsil+co^2*Wp^2*epsil^2+2*co*Wp*L(2)*sqrt(Ek)+Wp^2*si^2-2*epsil*co*Wp*L(2)*sqrt(Ek)+co^2*Wp^2+2*epsil*co*Wp-2*co^2*Wp^2*epsil+L(1)^2*sqrt(Ek)^2+L(2)^2*sqrt(Ek)^2)/Wp/si/(1+epsil) -(L(1)^2*sqrt(Ek)^2+2*co*Wp*L(2)*sqrt(Ek)+L(2)^2*sqrt(Ek)^2+co^2*Wp^2-2*epsil*co*Wp*L(2)*sqrt(Ek)-2*co^2*Wp^2*epsil+co^2*Wp^2*epsil^2)/Wp/si/(1+epsil)];
                r = roots(polynom);
            elseif(choice==1)
                %                 if(eta==0); Nroots=8; else Nroots=14; end;
                polynom=[eta^2 0 0 0 (-eta^2+2*eta*Wp*co) 2*Ek^(1/2)*L(2)*eta Ek*L(1)^2 0 (Wp^2.*co^2-2*eta*Wp*co+Wp^2*si^2) ...
                    (2*Ek^(1/2)*L(2)*Wp*co-2*Ek^(1/2)*L(2)*eta) (-Ek*L(1)^2+Ek*L(2)^2) 0 -Wp^2*co^2 -2*Ek^(1/2)*L(2)*Wp*co -Ek*L(2)^2];
                r = roots(polynom);
            end
            TolI=1e-14; TolR=1e-3; flag2=0;
            racine=[0]; % will allow to discard solutions too closed from 0
            
            while((flag2==0)&&(TolI<=1e-4)&&(TolR>=1e-13))
                for kk=1:length(r)
                    if(choice==0)
                        C0=r(kk);
                        A0=-(C0-1).*(co.*Wp-epsil.*co.*Wp-epsil.*C0+L(2).*sqrt(Ek))./(Wp.*si.*(1+epsil));
                        B0=L(1).*sqrt(Ek).*(C0-1)./(Wp.*si.*(1+epsil));
                    elseif(choice==1)
                        x=r(kk);
                        X=sqrt(Ek/x^2)*L(2)+eta*x^4+Wp*co;
                        Y=-sqrt(Ek*x^2)*L(1);
                        F0 = Wp^2*si^2/(1/x^4-1)-X^2-Y^2;
                        C0=r(kk)^4; A0=X*(1-C0)/(Wp*si); B0=Y*(1-C0)/(Wp*si);
                    end
                    if(sqrt(imag(A0)^2+imag(B0)^2+imag(C0)^2)<=TolI && abs(real(C0))>=TolR)
                        compteur=compteur+1;
                        A0=real(A0); B0=real(B0); C0=real(C0);
                        flagij=0;
                        for kkh=1:length(racine)
                            if(abs(C0-racine(kkh))<=5e-12), flagij=1; end;
                        end
                        if(choice==0)
                            if((abs(Wp*co*B0-epsil*(Wp*co*B0+B0*C0)+L(1)*sqrt(Ek)*A0+L(2)*B0*sqrt(Ek))<=1e-10) && (abs(Wp*si*C0-Wp*co*A0+epsil*(Wp*co*A0+A0*C0)+L(1)*sqrt(Ek)*B0-L(2)*A0*sqrt(Ek))<=1e-10) ...
                                    && (abs(-Wp*si*B0-epsil*Wp*si*B0+L(1)*sqrt(Ek)*(C0-1))<=1e-10) &&(flagij==0))
                                flag=1; flag2=1;
                                A=[A A0*cos(phi)-B0*sin(phi)]; B=[B A0*sin(phi)+B0*cos(phi)]; C=[C C0]; racine=[racine C0];
                                eq1=abs(Wp*co*B0-epsil*(Wp*co*B0+B0*C0)+L(1)*sqrt(Ek)*A0+L(2)*B0*sqrt(Ek));
                                eq2=abs(Wp*si*C0-Wp*co*A0+epsil*(Wp*co*A0+A0*C0)+L(1)*sqrt(Ek)*B0-L(2)*A0*sqrt(Ek));
                                eq3=abs(-Wp*si*B0-epsil*Wp*si*B0+L(1)*sqrt(Ek)*(C0-1));
                            end
                        elseif(choice==1)
                            if((abs(F0)<=1e-5) && (abs(A0^2+B0^2+C0^2-C0)<=1e-10) && abs(Wpz*B0+eta*B0*C0+sqrt(Ek)*(L(1)*A0*C0^(1/4)+L(2)*B0*C0^(-1/4)))<=1e-10 ...
                                    && abs(Wp*si*B0+L(1)*C0^(1/4)*(1-C0)*sqrt(Ek))<=1e-10 && (flagij==0)) % WARNING : Wp*si = sign(Wp)*sqrt(Wpx^2+Wpy^2) <> sqrt(Wpx^2+Wpy^2)
                                flag=1; flag2=1;
                                A=[A A0*cos(phi)-B0*sin(phi)]; B=[B A0*sin(phi)+B0*cos(phi)]; C=[C C0]; racine=[racine C0];
                                eq1=abs(A.^2+B.^2+C.^2-C);
                                eq2=abs(-Wpy.*C+Wpz.*B+eta.*B.*C+sqrt(Ek).*(L(1).*A.*C.^(1./4)+L(2).*B.*C.^(-1./4)));
                                eq3=abs(-Wpx.*B+Wpy.*A-L(1).*C.^(1./4).*(1-C).*sqrt(Ek));
                            end
                        end
                    end
                end
                if(flag2==0), TolI=TolI*10; TolR=TolR/10; end;
            end
            
        elseif((choice==2)||(choice==5)) % Cebron et al. 2010
            if(details==1), display('Cebron et al. 2010'); end;
            if(choice==2)
                eta2=1-b/a; ee=eta-eta2; Wp1=Wpx; Wp2=Wpy; Wp3=Wpz; xi=1;
            elseif(choice==5)
                eta2= 2*c^2*(1/(c^2+b^2)-1/(c^2+a^2));
                ee=   2*a^2*(1/(a^2+c^2)-1/(a^2+b^2));
                Wp1=2*c^2/(a^2+c^2)*Wpx;
                Wp2=2*c^2/(b^2+c^2)*Wpy;
                Wp3=2*a^2/(a^2+c^2)*Wpz;
                xi=a^2/c^2*(b^2+c^2)/(b^2+a^2);
            end
            % % Eq. for Cebron et al. 2010 (without xi)
            %             eqtot=@(w3) (Ek*L(1)^2*w3+Wp3^2*w3^(1/2)+2*Wp3*w3^(3/2)*ee+2*Wp3*w3^(1/4)*Ek^(1/2)*L(2)+ee^2*w3^(5/2)+2*ee*w3^(5/4)*Ek^(1/2)*L(2)+Ek*L(2)^2)*(Ek^(3/2)*L(1)^2*Wp3^4*w3+Wp2^4*w3^(5/2)*Ek^(1/2)*Wp3^2+Wp2^4*w3^(9/2)*Ek^(1/2)*ee^2+Wp2^4*w3^2*Ek^(3/2)*L(2)^2-4*Wp2^2*Ek*w3^(5/4)*Wp3^3*L(2)-6*Wp2^2*Ek^(3/2)*w3*Wp3^2*L(2)^2-4*Wp2^2*Ek^2*w3^(3/4)*Wp3*L(2)^3+Wp2^2*Ek^(1/2)*w3^(5/2)*Wp3^4+Wp2^2*Ek^(5/2)*w3^(3/2)*L(2)^4+Wp2^2*Ek^(1/2)*w3^(13/2)*ee^4+L(1)^6*w3^2*Ek^(7/2)+L(1)^6*w3^4*Ek^(7/2)+eta2^2*Wp2^4*w3^(9/2)*Ek^(1/2)-w3^4*eta2^2*Wp2^2*Ek^(3/2)*L(1)^2+4*Ek^2*L(1)^2*ee^3*w3^(15/4)*L(2)+4*w3^(23/4)*Ek^2*L(1)^2*ee^3*L(2)+4*Wp2^2*Ek*w3^(21/4)*ee^3*L(2)+6*Wp2^2*Ek^(3/2)*w3^4*ee^2*L(2)^2+4*Wp2^2*Ek^2*w3^(11/4)*ee*L(2)^3+4*Wp2^2*Ek^(1/2)*w3^(7/2)*Wp3^3*ee+4*Wp2^2*Ek*w3^(9/4)*Wp3^3*L(2)+6*Wp2^2*Ek^(1/2)*w3^(9/2)*Wp3^2*ee^2+6*Wp2^2*Ek^(3/2)*w3^2*Wp3^2*L(2)^2+4*Wp2^2*Ek^(1/2)*w3^(11/2)*Wp3*ee^3+4*Wp2^2*Ek^2*w3^(7/4)*Wp3*L(2)^3-2*Wp2^2*w3^(5/2)*eta2*Ek^(1/2)*Wp3^3-2*Wp2^2*w3^(11/2)*eta2*Ek^(1/2)*ee^3-2*Wp2^2*w3^(7/4)*eta2*Ek^2*L(2)^3-8*Ek^2*L(1)^2*w3^(19/4)*ee^3*L(2)+2*eta2*w3^(13/4)*Ek^3*L(1)^2*L(2)^3+2*eta2*w3^(5/2)*Ek^(5/2)*L(1)^4*Wp3+2*eta2*w3^(7/2)*Ek^(5/2)*L(1)^4*ee+2*eta2*w3^(9/4)*Ek^3*L(1)^4*L(2)-4*eta2*w3^(7/2)*Ek^(5/2)*L(1)^4*Wp3-4*eta2*w3^(9/2)*Ek^(5/2)*L(1)^4*ee-4*eta2*w3^(13/4)*Ek^3*L(1)^4*L(2)+2*eta2*w3^(9/2)*Ek^(5/2)*L(1)^4*Wp3+2*eta2*w3^(11/2)*Ek^(5/2)*L(1)^4*ee...
            %                 +2*eta2*w3^(17/4)*Ek^3*L(1)^4*L(2)+4*Ek^(5/2)*L(1)^4*w3^(5/2)*Wp3*ee-2*Wp2^3*w3^(15/4)*Wp1*eta2*Ek*L(1)+2*w3^(13/4)*Wp1*eta2*Wp2*Ek^2*L(1)^3-2*w3^(17/4)*Wp1*eta2*Wp2*Ek^2*L(1)^3+Wp2^4*w3^3*Ek^(3/2)*L(1)^2-12*eta2*w3^4*Ek^(3/2)*L(1)^2*Wp3^2*ee-12*eta2*w3^(11/4)*Ek^2*L(1)^2*Wp3^2*L(2)-12*eta2*w3^5*Ek^(3/2)*L(1)^2*Wp3*ee^2-12*eta2*w3^(5/2)*Ek^(5/2)*L(1)^2*Wp3*L(2)^2-12*eta2*w3^(19/4)*Ek^2*L(1)^2*ee^2*L(2)-12*eta2*w3^(7/2)*Ek^(5/2)*L(1)^2*ee*L(2)^2+12*Ek^2*L(1)^2*Wp3^2*w3^(7/4)*ee*L(2)+12*Ek^2*L(1)^2*Wp3*w3^(11/4)*ee^2*L(2)+12*Ek^(5/2)*L(1)^2*Wp3*w3^(3/2)*ee*L(2)^2+2*w3^4*Wp1^2*Ek^(3/2)*L(1)^2*Wp3*ee+2*w3^(11/4)*Wp1^2*Ek^2*L(1)^2*Wp3*L(2)+2*w3^(15/4)*Wp1^2*Ek^2*L(1)^2*ee*L(2)+6*w3^4*Wp2^2*Ek^(3/2)*L(1)^2*Wp3*ee+6*w3^(11/4)*Wp2^2*Ek^2*L(1)^2*Wp3*L(2)+6*w3^(15/4)*Wp2^2*Ek^2*L(1)^2*ee*L(2)+2*eta2^2*w3^4*Ek^(3/2)*L(1)^2*Wp3*ee+2*eta2^2*w3^(11/4)*Ek^2*L(1)^2*Wp3*L(2)+2*eta2^2*w3^(15/4)*Ek^2*L(1)^2*ee*L(2)-4*eta2^2*w3^5*Ek^(3/2)*L(1)^2*Wp3*ee-4*eta2^2*w3^(15/4)*Ek^2*L(1)^2*Wp3*L(2)-4*eta2^2*w3^(19/4)*Ek^2*L(1)^2*ee*L(2)+2*eta2^2*w3^6*Ek^(3/2)*L(1)^2*Wp3*ee+2*eta2^2*w3^(19/4)*Ek^2*L(1)^2*Wp3*L(2)+2*eta2^2*w3^(23/4)*Ek^2*L(1)^2*ee*L(2)-12*Wp2^2*Ek*w3^(9/4)*Wp3^2*ee*L(2)+6*Wp2^2*w3^(9/2)*eta2*Ek^(1/2)*Wp3^2*ee-4*w3^3*eta2*Wp2^2*Ek^(3/2)*L(1)^2*Wp3-4*w3^4*eta2*Wp2^2*Ek^(3/2)*L(1)^2*ee-4*w3^(11/4)*eta2*Wp2^2*Ek^2*L(1)^2*L(2)+4*w3^4*eta2*Wp2^2*Ek^(3/2)*L(1)^2*Wp3...
            %                 +4*w3^5*eta2*Wp2^2*Ek^(3/2)*L(1)^2*ee+4*w3^(15/4)*eta2*Wp2^2*Ek^2*L(1)^2*L(2)+6*eta2*Ek^(3/2)*L(1)^2*w3^3*Wp3^2*ee+6*eta2*Ek^2*L(1)^2*w3^(7/4)*Wp3^2*L(2)+6*eta2*Ek^(3/2)*L(1)^2*w3^4*Wp3*ee^2+6*eta2*Ek^(5/2)*L(1)^2*w3^(3/2)*Wp3*L(2)^2+6*eta2*Ek^2*L(1)^2*w3^(15/4)*ee^2*L(2)+6*eta2*Ek^(5/2)*L(1)^2*w3^(5/2)*ee*L(2)^2+w3^3*Ek^(3/2)*L(1)^2*Wp3^4+w3^2*Ek^(7/2)*L(1)^2*L(2)^4+w3^7*Ek^(3/2)*L(1)^2*ee^4+Wp2^2*w3^(5/2)*Wp1^2*Ek^(1/2)*Wp3^2+Wp2^2*w3^(9/2)*Wp1^2*Ek^(1/2)*ee^2+Wp2^2*w3^2*Wp1^2*Ek^(3/2)*L(2)^2+12*w3^(15/4)*Ek^2*L(1)^2*Wp3^2*ee*L(2)+12*w3^(19/4)*Ek^2*L(1)^2*Wp3*ee^2*L(2)+12*w3^(7/2)*Ek^(5/2)*L(1)^2*Wp3*ee*L(2)^2-24*Ek^2*L(1)^2*w3^(11/4)*Wp3^2*ee*L(2)-24*Ek^2*L(1)^2*w3^(15/4)*Wp3*ee^2*L(2)-24*Ek^(5/2)*L(1)^2*w3^(5/2)*Wp3*ee*L(2)^2+12*Wp2^2*Ek*w3^(13/4)*Wp3^2*ee*L(2)-2*Wp1^2*Ek^(3/2)*L(1)^2*w3^3*Wp3*ee-2*Wp1^2*Ek^2*L(1)^2*w3^(7/4)*Wp3*L(2)-2*Wp1^2*Ek^2*L(1)^2*w3^(11/4)*ee*L(2)-6*Wp2^2*Ek^(3/2)*L(1)^2*w3^3*Wp3*ee-6*Wp2^2*Ek^2*L(1)^2*w3^(7/4)*Wp3*L(2)-6*Wp2^2*Ek^2*L(1)^2*w3^(11/4)*ee*L(2)-6*Wp2^2*w3^(7/2)*eta2*Ek^(1/2)*Wp3^2*ee+6*eta2*w3^5*Ek^(3/2)*L(1)^2*Wp3^2*ee+6*eta2*w3^(15/4)*Ek^2*L(1)^2*Wp3^2*L(2)+6*eta2*w3^6*Ek^(3/2)*L(1)^2*Wp3*ee^2+6*eta2*w3^(7/2)*Ek^(5/2)*L(1)^2*Wp3*L(2)^2+6*eta2*w3^(23/4)*Ek^2*L(1)^2*ee^2*L(2)+6*eta2*w3^(9/2)*Ek^(5/2)*L(1)^2*ee*L(2)^2+2*Wp2^2*w3^(7/2)*Wp1^2*Ek^(1/2)*Wp3*ee+2*Wp2^2*w3^(9/4)*Wp1^2*Ek*Wp3*L(2)+2*Wp2^2*w3^(13/4)*Wp1^2*Ek*ee*L(2)...
            %                 -12*Wp2^2*Ek*w3^(13/4)*Wp3*ee^2*L(2)-12*Wp2^2*Ek^(3/2)*w3^2*Wp3*ee*L(2)^2+6*Wp2^2*w3^(13/4)*eta2*Ek*Wp3^2*L(2)+6*Wp2^2*w3^(11/2)*eta2*Ek^(1/2)*Wp3*ee^2+6*Wp2^2*w3^3*eta2*Ek^(3/2)*Wp3*L(2)^2+6*Wp2^2*w3^(21/4)*eta2*Ek*ee^2*L(2)+6*Wp2^2*w3^4*eta2*Ek^(3/2)*ee*L(2)^2-2*eta2^2*w3^(9/2)*Wp2^2*Ek^(1/2)*Wp3*ee-2*eta2^2*w3^(13/4)*Wp2^2*Ek*Wp3*L(2)-2*eta2^2*w3^(17/4)*Wp2^2*Ek*ee*L(2)+2*eta2^2*w3^(11/2)*Wp2^2*Ek^(1/2)*Wp3*ee+2*eta2^2*w3^(17/4)*Wp2^2*Ek*Wp3*L(2)+2*eta2^2*w3^(21/4)*Wp2^2*Ek*ee*L(2)+12*Wp2^2*Ek*w3^(17/4)*Wp3*ee^2*L(2)+12*Wp2^2*Ek^(3/2)*w3^3*Wp3*ee*L(2)^2-6*Wp2^2*w3^(9/4)*eta2*Ek*Wp3^2*L(2)-6*Wp2^2*w3^(9/2)*eta2*Ek^(1/2)*Wp3*ee^2-6*Wp2^2*w3^2*eta2*Ek^(3/2)*Wp3*L(2)^2-6*Wp2^2*w3^(17/4)*eta2*Ek*ee^2*L(2)-6*Wp2^2*w3^3*eta2*Ek^(3/2)*ee*L(2)^2+eta2^2*w3^(9/2)*Wp2^2*Ek^(1/2)*Wp3^2+eta2^2*w3^(13/2)*Wp2^2*Ek^(1/2)*ee^2+eta2^2*w3^4*Wp2^2*Ek^(3/2)*L(2)^2+w3^(7/2)*Wp1^2*Ek^(5/2)*L(1)^4+Ek^(3/2)*L(1)^2*ee^4*w3^5+w3^5*eta2^2*Wp2^2*Ek^(3/2)*L(1)^2+Wp2^2*w3^3*Wp1^2*Ek^(3/2)*L(1)^2-24*eta2*w3^(15/4)*Ek^2*L(1)^2*Wp3*ee*L(2)+12*Wp2^2*w3^(17/4)*eta2*Ek*Wp3*ee*L(2)+12*eta2*Ek^2*L(1)^2*w3^(11/4)*Wp3*ee*L(2)-12*Wp2^2*w3^(13/4)*eta2*Ek*Wp3*ee*L(2)+12*eta2*w3^(19/4)*Ek^2*L(1)^2*Wp3*ee*L(2)+w3^3*Wp1^2*Ek^(3/2)*L(1)^2*Wp3^2+w3^5*Wp1^2*Ek^(3/2)*L(1)^2*ee^2+w3^(5/2)*Wp1^2*Ek^(5/2)*L(1)^2*L(2)^2+eta2^2*w3^3*Ek^(3/2)*L(1)^2*Wp3^2+eta2^2*w3^5*Ek^(3/2)*L(1)^2*ee^2+...
            %                 eta2^2*w3^(5/2)*Ek^(5/2)*L(1)^2*L(2)^2+eta2^2*w3^5*Ek^(3/2)*L(1)^2*Wp3^2+eta2^2*w3^7*Ek^(3/2)*L(1)^2*ee^2+eta2^2*w3^(9/2)*Ek^(5/2)*L(1)^2*L(2)^2-Wp2^2*Ek^(1/2)*w3^(11/2)*ee^4-2*Ek^(7/2)*L(1)^2*w3*L(2)^4+2*Ek^(7/2)*L(1)^4*w3*L(2)^2-2*Ek^(3/2)*L(1)^2*w3^2*Wp3^4-2*Ek^(3/2)*L(1)^2*w3^6*ee^4+2*Ek^(5/2)*L(1)^4*w3^(3/2)*Wp3^2+2*Ek^(5/2)*L(1)^4*w3^(7/2)*ee^2-4*w3^(5/2)*Ek^(5/2)*L(1)^4*Wp3^2-4*w3^(9/2)*Ek^(5/2)*L(1)^4*ee^2-4*w3^2*Ek^(7/2)*L(1)^4*L(2)^2+2*w3^(7/2)*Ek^(5/2)*L(1)^4*Wp3^2+2*w3^(11/2)*Ek^(5/2)*L(1)^4*ee^2+2*w3^3*Ek^(7/2)*L(1)^4*L(2)^2+Ek^(7/2)*L(1)^2*L(2)^4-Wp2^2*Ek^(1/2)*w3^(3/2)*Wp3^4-Wp2^2*Ek^(5/2)*w3^(1/2)*L(2)^4-w3^(5/2)*Wp1^2*Ek^(5/2)*L(1)^4+2*Wp2^2*w3^(7/2)*Ek^(5/2)*L(1)^4-2*Wp2^2*w3^(5/2)*Ek^(5/2)*L(1)^4+4*Ek^3*L(1)^4*w3^(5/4)*Wp3*L(2)+4*Ek^3*L(1)^4*w3^(9/4)*ee*L(2)-8*w3^(7/2)*Ek^(5/2)*L(1)^4*Wp3*ee-8*w3^(9/4)*Ek^3*L(1)^4*Wp3*L(2)-8*w3^(13/4)*Ek^3*L(1)^4*ee*L(2)+4*w3^(9/2)*Ek^(5/2)*L(1)^4*Wp3*ee+4*w3^(13/4)*Ek^3*L(1)^4*Wp3*L(2)+4*w3^(17/4)*Ek^3*L(1)^4*ee*L(2)-4*eta2*w3^3*Ek^(3/2)*L(1)^2*Wp3^3-4*eta2*w3^6*Ek^(3/2)*L(1)^2*ee^3-4*eta2*w3^(9/4)*Ek^3*L(1)^2*L(2)^3+6*Ek^(5/2)*L(1)^2*ee^2*w3^(5/2)*L(2)^2+4*Ek^3*L(1)^2*ee*w3^(5/4)*L(2)^3+4*Ek^(3/2)*L(1)^2*Wp3^3*w3^2*ee+6*Ek^(3/2)*L(1)^2*Wp3^2*w3^3*ee^2+4*Ek^(3/2)*L(1)^2*Wp3*w3^4*ee^3+3*w3^3*Wp2^2*Ek^(3/2)*L(1)^2*Wp3^2+3*w3^5*Wp2^2*Ek^(3/2)*L(1)^2*ee^2+3*w3^(5/2)*Wp2^2*Ek^(5/2)*L(1)^2*L(2)^2-2*eta2^2*w3^4*Ek^(3/2)*L(1)^2*Wp3^2-...
            %                 2*eta2^2*w3^6*Ek^(3/2)*L(1)^2*ee^2-2*eta2^2*w3^(7/2)*Ek^(5/2)*L(1)^2*L(2)^2+2*eta2*Ek^(3/2)*L(1)^2*w3^2*Wp3^3+2*eta2*Ek^(3/2)*L(1)^2*w3^5*ee^3+2*eta2*Ek^3*L(1)^2*w3^(5/4)*L(2)^3+6*w3^(9/2)*Ek^(5/2)*L(1)^2*ee^2*L(2)^2+4*w3^(13/4)*Ek^3*L(1)^2*ee*L(2)^3+4*w3^4*Ek^(3/2)*L(1)^2*Wp3^3*ee+4*w3^(11/4)*Ek^2*L(1)^2*Wp3^3*L(2)+6*w3^5*Ek^(3/2)*L(1)^2*Wp3^2*ee^2+6*w3^(5/2)*Ek^(5/2)*L(1)^2*Wp3^2*L(2)^2+4*w3^6*Ek^(3/2)*L(1)^2*Wp3*ee^3+4*w3^(9/4)*Ek^3*L(1)^2*Wp3*L(2)^3-12*Ek^(5/2)*L(1)^2*w3^(7/2)*ee^2*L(2)^2-8*Ek^3*L(1)^2*w3^(9/4)*ee*L(2)^3-8*Ek^(3/2)*L(1)^2*w3^3*Wp3^3*ee-8*Ek^2*L(1)^2*w3^(7/4)*Wp3^3*L(2)-12*Ek^(3/2)*L(1)^2*w3^4*Wp3^2*ee^2-12*Ek^(5/2)*L(1)^2*w3^(3/2)*Wp3^2*L(2)^2-8*Ek^(3/2)*L(1)^2*w3^5*Wp3*ee^3-8*Ek^3*L(1)^2*w3^(5/4)*Wp3*L(2)^3-Wp1^2*Ek^(3/2)*L(1)^2*w3^2*Wp3^2-Wp1^2*Ek^(3/2)*L(1)^2*w3^4*ee^2-Wp1^2*Ek^(5/2)*L(1)^2*w3^(3/2)*L(2)^2-3*Wp2^2*Ek^(3/2)*L(1)^2*w3^2*Wp3^2-3*Wp2^2*Ek^(3/2)*L(1)^2*w3^4*ee^2-3*Wp2^2*Ek^(5/2)*L(1)^2*w3^(3/2)*L(2)^2+2*eta2*w3^4*Ek^(3/2)*L(1)^2*Wp3^3+2*eta2*w3^7*Ek^(3/2)*L(1)^2*ee^3+2*Wp2^2*w3^(7/2)*eta2*Ek^(1/2)*Wp3^3+2*Wp2^2*w3^(13/2)*eta2*Ek^(1/2)*ee^3+2*Wp2^2*w3^(11/4)*eta2*Ek^2*L(2)^3-eta2^2*w3^(7/2)*Wp2^2*Ek^(1/2)*Wp3^2-eta2^2*w3^(11/2)*Wp2^2*Ek^(1/2)*ee^2-eta2^2*w3^3*Wp2^2*Ek^(3/2)*L(2)^2+4*Ek^2*L(1)^2*Wp3^3*w3^(3/4)*L(2)+6*Ek^(5/2)*L(1)^2*Wp3^2*w3^(1/2)*L(2)^2+4*Ek^3*L(1)^2*Wp3*w3^(1/4)*L(2)^3+2*Wp2^4*w3^(7/2)*Ek^(1/2)*Wp3*ee+2*Wp2^4*w3^(9/4)*Ek*Wp3*L(2)...
            %                 +2*Wp2^4*w3^(13/4)*Ek*ee*L(2)-4*Wp2^2*Ek*w3^(17/4)*ee^3*L(2)-6*Wp2^2*Ek^(3/2)*w3^3*ee^2*L(2)^2-4*Wp2^2*Ek^2*w3^(7/4)*ee*L(2)^3-4*Wp2^2*Ek^(1/2)*w3^(5/2)*Wp3^3*ee-6*Wp2^2*Ek^(1/2)*w3^(7/2)*Wp3^2*ee^2-4*Wp2^2*Ek^(1/2)*w3^(9/2)*Wp3*ee^3+2*Wp2^4*w3^(7/2)*eta2*Ek^(1/2)*Wp3+2*Wp2^4*w3^(9/2)*eta2*Ek^(1/2)*ee+2*Wp2^4*w3^(13/4)*eta2*Ek*L(2)-2*L(1)^6*w3^3*Ek^(7/2))*w3^(1/2);
            
            lr=L(1); li=L(2);
            eqtot=@(w3) real((Ek*lr^2*w3+Wp3^2*w3^(1/2)+2*Wp3*w3^(3/2)*ee+2*Wp3*w3^(1/4)*Ek^(1/2)*li+ee^2*w3^(5/2)+2*ee*w3^(5/4)*Ek^(1/2)*li+Ek*li^2)*(lr^6*w3^2*Ek^(7/2)+4*Ek^2*lr^2*Wp3^3*w3^(3/4)*li+6*Wp2^2*w3^4*Ek^(3/2)*ee^2*li^2+4*Wp2^2*w3^(11/4)*Ek^2*ee*li^3+4*Wp2^2*w3^(7/2)*Ek^(1/2)*Wp3^3*ee+4*Wp2^2*w3^(9/4)*Ek*Wp3^3*li+6*Wp2^2*w3^(9/2)*Ek^(1/2)*Wp3^2*ee^2+6*Wp2^2*w3^2*Ek^(3/2)*Wp3^2*li^2+4*Wp2^2*w3^(11/2)*Ek^(1/2)*Wp3*ee^3+4*Wp2^2*w3^(7/4)*Ek^2*Wp3*li^3+4*Wp2^2*w3^(21/4)*Ek*ee^3*li+6*Ek^(5/2)*lr^2*Wp3^2*w3^(1/2)*li^2+4*Ek^3*lr^2*Wp3*w3^(1/4)*li^3-2*Wp2^2*xi*w3^(5/2)*Ek^(5/2)*lr^4+2*Wp2^2*xi*w3^(7/2)*Ek^(5/2)*lr^4-8*w3^(7/2)*Ek^(5/2)*lr^4*Wp3*ee-8*w3^(9/4)*Ek^3*lr^4*Wp3*li-8*w3^(13/4)*Ek^3*lr^4*ee*li+4*w3^(9/2)*Ek^(5/2)*lr^4*Wp3*ee+4*w3^(13/4)*Ek^3*lr^4*Wp3*li+4*w3^(17/4)*Ek^3*lr^4*ee*li+2*eta2*w3^(9/2)*Ek^(5/2)*lr^4*Wp3+2*eta2*w3^(11/2)*Ek^(5/2)*lr^4*ee+2*eta2*w3^(17/4)*Ek^3*lr^4*li-4*eta2*w3^(7/2)*Ek^(5/2)*lr^4*Wp3-4*eta2*w3^(9/2)*Ek^(5/2)*lr^4*ee-4*eta2*w3^(13/4)*Ek^3*lr^4*li+2*eta2*w3^(5/2)*Ek^(5/2)*lr^4*Wp3+2*eta2*w3^(7/2)*Ek^(5/2)*lr^4*ee+2*eta2*w3^(9/4)*Ek^3*lr^4*li+4*Ek^(5/2)*lr^4*w3^(5/2)*Wp3*ee+4*Ek^3*lr^4*w3^(5/4)*Wp3*li+4*Ek^3*lr^4*w3^(9/4)*ee*li-8*Ek^2*lr^2*w3^(7/4)*Wp3^3*li-12*Ek^(3/2)*lr^2*w3^4*Wp3^2*ee^2-12*Ek^(5/2)*lr^2*w3^(3/2)*Wp3^2*li^2-8*Ek^(3/2)*lr^2*w3^5*Wp3*ee^3-8*Ek^3*lr^2*w3^(5/4)*Wp3*li^3-8*Ek^2*lr^2*w3^(19/4)*ee^3*li-12*Ek^(5/2)*lr^2*w3^(7/2)*ee^2*li^2-...
                8*Ek^3*lr^2*w3^(9/4)*ee*li^3+4*w3^(11/4)*Ek^2*lr^2*Wp3^3*li+6*w3^5*Ek^(3/2)*lr^2*Wp3^2*ee^2+6*w3^(5/2)*Ek^(5/2)*lr^2*Wp3^2*li^2+4*w3^6*Ek^(3/2)*lr^2*Wp3*ee^3+4*w3^(9/4)*Ek^3*lr^2*Wp3*li^3+4*w3^(23/4)*Ek^2*lr^2*ee^3*li+6*w3^(9/2)*Ek^(5/2)*lr^2*ee^2*li^2+4*w3^(13/4)*Ek^3*lr^2*ee*li^3-Wp1^2*Ek^(3/2)*lr^2*w3^2*Wp3^2-Wp1^2*Ek^(3/2)*lr^2*w3^4*ee^2-Wp1^2*Ek^(5/2)*lr^2*w3^(3/2)*li^2-Wp2^2*Ek^(3/2)*lr^2*w3^2*Wp3^2-Wp2^2*Ek^(3/2)*lr^2*w3^4*ee^2-Wp2^2*Ek^(5/2)*lr^2*w3^(3/2)*li^2+6*Ek^(3/2)*lr^2*Wp3^2*w3^3*ee^2+4*Ek^(3/2)*lr^2*Wp3*w3^4*ee^3+4*Ek^2*lr^2*ee^3*w3^(15/4)*li+6*Ek^(5/2)*lr^2*ee^2*w3^(5/2)*li^2+4*Ek^3*lr^2*ee*w3^(5/4)*li^3-2*eta2^2*w3^4*Ek^(3/2)*lr^2*Wp3^2-2*eta2^2*w3^6*Ek^(3/2)*lr^2*ee^2-2*eta2^2*w3^(7/2)*Ek^(5/2)*lr^2*li^2+2*eta2*Ek^(3/2)*lr^2*w3^2*Wp3^3+2*eta2*Ek^(3/2)*lr^2*w3^5*ee^3+2*eta2*Ek^3*lr^2*w3^(5/4)*li^3-4*eta2*w3^3*Ek^(3/2)*lr^2*Wp3^3-4*eta2*w3^6*Ek^(3/2)*lr^2*ee^3-4*eta2*w3^(9/4)*Ek^3*lr^2*li^3+2*eta2*w3^4*Ek^(3/2)*lr^2*Wp3^3+2*eta2*w3^7*Ek^(3/2)*lr^2*ee^3+2*eta2*w3^(13/4)*Ek^3*lr^2*li^3+lr^6*w3^4*Ek^(7/2)-8*Ek^(3/2)*lr^2*w3^3*Wp3^3*ee+Wp2^2*w3^(5/2)*Ek^(1/2)*Wp3^4+Wp2^2*w3^(3/2)*Ek^(5/2)*li^4+Wp2^2*w3^(13/2)*Ek^(1/2)*ee^4+Ek^(3/2)*lr^2*Wp3^4*w3-4*Wp2^2*w3^(5/4)*Ek*Wp3^3*li-6*Wp2^2*w3*Ek^(3/2)*Wp3^2*li^2-4*Wp2^2*w3^(3/4)*Ek^2*Wp3*li^3+4*w3^4*Ek^(3/2)*lr^2*Wp3^3*ee+4*Ek^(3/2)*lr^2*Wp3^3*w3^2*ee-4*Wp2^2*w3^(5/2)*Ek^(1/2)*Wp3^3*ee-6*Wp2^2*w3^(7/2)*Ek^(1/2)*Wp3^2*ee^2-...
                4*Wp2^2*w3^(9/2)*Ek^(1/2)*Wp3*ee^3-4*Wp2^2*w3^(17/4)*Ek*ee^3*li-6*Wp2^2*w3^3*Ek^(3/2)*ee^2*li^2-4*Wp2^2*w3^(7/4)*Ek^2*ee*li^3+Ek^(3/2)*lr^2*ee^4*w3^5+w3^(7/2)*Wp1^2*Ek^(5/2)*lr^4+w3^3*Ek^(3/2)*lr^2*Wp3^4+w3^2*Ek^(7/2)*lr^2*li^4+w3^7*Ek^(3/2)*lr^2*ee^4+12*Wp2^2*w3^(17/4)*Ek*Wp3*ee^2*li+12*Wp2^2*w3^3*Ek^(3/2)*Wp3*ee*li^2-12*Wp2^2*w3^(13/4)*Ek*Wp3*ee^2*li-12*Wp2^2*w3^2*Ek^(3/2)*Wp3*ee*li^2-2*w3^(9/4)*Wp1*Wp2*Ek^2*lr^3*Wp3-2*w3^(13/4)*Wp1*Wp2*Ek^2*lr^3*ee+2*Wp2^4*xi^2*w3^(7/2)*Ek^(1/2)*Wp3*ee+2*Wp2^4*xi^2*w3^(9/4)*Ek*Wp3*li+2*Wp2^4*xi^2*w3^(13/4)*Ek*ee*li-2*Wp2^2*xi*w3^(5/2)*eta2*Ek^(1/2)*Wp3^3-2*Wp2^2*xi*w3^(11/2)*eta2*Ek^(1/2)*ee^3-2*Wp2^2*xi*w3^(7/4)*eta2*Ek^2*li^3-2*Wp1*Wp2*Ek*lr*w3^(19/4)*ee^3-2*Wp1*Wp2*Ek^(5/2)*lr*w3*li^3+2*w3^(13/4)*Wp1*Wp2*Ek^2*lr^3*Wp3+2*w3^(17/4)*Wp1*Wp2*Ek^2*lr^3*ee+2*w3^(11/4)*Wp1*Wp2*Ek*lr*Wp3^3+2*w3^(23/4)*Wp1*Wp2*Ek*lr*ee^3+2*Wp2^2*xi*w3^(7/2)*eta2*Ek^(1/2)*Wp3^3+2*Wp2^2*xi*w3^(13/2)*eta2*Ek^(1/2)*ee^3+2*Wp2^2*xi*w3^(11/4)*eta2*Ek^2*li^3+2*Wp2^4*xi^3*w3^(7/2)*eta2*Ek^(1/2)*Wp3+2*Wp2^4*xi^3*w3^(9/2)*eta2*Ek^(1/2)*ee+2*Wp2^4*xi^3*w3^(13/4)*eta2*Ek*li-eta2^2*w3^(7/2)*Wp2^2*xi^2*Ek^(1/2)*Wp3^2-eta2^2*w3^(11/2)*Wp2^2*xi^2*Ek^(1/2)*ee^2-eta2^2*w3^3*Wp2^2*xi^2*Ek^(3/2)*li^2+eta2^2*Wp2^4*xi^4*w3^(9/2)*Ek^(1/2)-w3^4*eta2^2*Wp2^2*xi^2*Ek^(3/2)*lr^2-24*Ek^2*lr^2*w3^(11/4)*Wp3^2*ee*li-24*Ek^2*lr^2*w3^(15/4)*Wp3*ee^2*li-24*Ek^(5/2)*lr^2*w3^(5/2)*Wp3*ee*li^2+...
                12*w3^(15/4)*Ek^2*lr^2*Wp3^2*ee*li+12*w3^(19/4)*Ek^2*lr^2*Wp3*ee^2*li+12*w3^(7/2)*Ek^(5/2)*lr^2*Wp3*ee*li^2-2*Wp1^2*Ek^(3/2)*lr^2*w3^3*Wp3*ee-2*Wp1^2*Ek^2*lr^2*w3^(7/4)*Wp3*li-2*Wp1^2*Ek^2*lr^2*w3^(11/4)*ee*li+Ek^(7/2)*lr^2*li^4-Wp2^2*w3^(3/2)*Ek^(1/2)*Wp3^4-Wp2^2*w3^(1/2)*Ek^(5/2)*li^4+2*Ek^(7/2)*lr^4*w3*li^2-w3^(5/2)*Wp1^2*Ek^(5/2)*lr^4-4*w3^(5/2)*Ek^(5/2)*lr^4*Wp3^2-4*w3^(9/2)*Ek^(5/2)*lr^4*ee^2-4*w3^2*Ek^(7/2)*lr^4*li^2+2*w3^(7/2)*Ek^(5/2)*lr^4*Wp3^2+2*w3^(11/2)*Ek^(5/2)*lr^4*ee^2+2*w3^3*Ek^(7/2)*lr^4*li^2+2*Ek^(5/2)*lr^4*w3^(3/2)*Wp3^2+2*Ek^(5/2)*lr^4*w3^(7/2)*ee^2-2*Ek^(3/2)*lr^2*w3^2*Wp3^4-2*Ek^(3/2)*lr^2*w3^6*ee^4-2*Ek^(7/2)*lr^2*w3*li^4-Wp2^2*w3^(11/2)*Ek^(1/2)*ee^4+eta2^2*w3^3*Ek^(3/2)*lr^2*Wp3^2+eta2^2*w3^5*Ek^(3/2)*lr^2*ee^2+eta2^2*w3^(5/2)*Ek^(5/2)*lr^2*li^2+eta2^2*w3^5*Ek^(3/2)*lr^2*Wp3^2+eta2^2*w3^7*Ek^(3/2)*lr^2*ee^2+eta2^2*w3^(9/2)*Ek^(5/2)*lr^2*li^2+w3^3*Wp1^2*Ek^(3/2)*lr^2*Wp3^2+w3^5*Wp1^2*Ek^(3/2)*lr^2*ee^2+w3^(5/2)*Wp1^2*Ek^(5/2)*lr^2*li^2+w3^3*Wp2^2*Ek^(3/2)*lr^2*Wp3^2+w3^5*Wp2^2*Ek^(3/2)*lr^2*ee^2+w3^(5/2)*Wp2^2*Ek^(5/2)*lr^2*li^2+Wp2^2*xi^2*w3^(5/2)*Wp1^2*Ek^(1/2)*Wp3^2+Wp2^2*xi^2*w3^(9/2)*Wp1^2*Ek^(1/2)*ee^2+Wp2^2*xi^2*w3^2*Wp1^2*Ek^(3/2)*li^2+eta2^2*w3^(9/2)*Wp2^2*xi^2*Ek^(1/2)*Wp3^2+eta2^2*w3^(13/2)*Wp2^2*xi^2*Ek^(1/2)*ee^2+eta2^2*w3^4*Wp2^2*xi^2*Ek^(3/2)*li^2+Wp2^4*xi^2*w3^3*Ek^(3/2)*lr^2-2*Wp2^3*xi^3*w3^(15/4)*Wp1*eta2*Ek*lr+...
                2*w3^(13/4)*Wp1*eta2*Wp2*xi*Ek^2*lr^3-2*w3^(17/4)*Wp1*eta2*Wp2*xi*Ek^2*lr^3+12*eta2*Ek^2*lr^2*w3^(11/4)*Wp3*ee*li-24*eta2*w3^(15/4)*Ek^2*lr^2*Wp3*ee*li-4*Wp2^2*xi*Ek^(3/2)*lr^2*w3^3*Wp3*ee-4*Wp2^2*xi*Ek^2*lr^2*w3^(7/4)*Wp3*li-4*Wp2^2*xi*Ek^2*lr^2*w3^(11/4)*ee*li+12*eta2*w3^(19/4)*Ek^2*lr^2*Wp3*ee*li+4*Wp2^2*xi*w3^4*Ek^(3/2)*lr^2*Wp3*ee+4*Wp2^2*xi*w3^(11/4)*Ek^2*lr^2*Wp3*li+4*Wp2^2*xi*w3^(15/4)*Ek^2*lr^2*ee*li-6*Wp2^2*xi*w3^(7/2)*eta2*Ek^(1/2)*Wp3^2*ee-6*Wp2^2*xi*w3^(9/4)*eta2*Ek*Wp3^2*li-6*Wp2^2*xi*w3^(9/2)*eta2*Ek^(1/2)*Wp3*ee^2-6*Wp2^2*xi*w3^2*eta2*Ek^(3/2)*Wp3*li^2-6*Wp2^2*xi*w3^(17/4)*eta2*Ek*ee^2*li-6*Wp2^2*xi*w3^3*eta2*Ek^(3/2)*ee*li^2+2*w3^(9/4)*Wp2*xi*Wp1*Ek^2*lr^3*Wp3+2*w3^(13/4)*Wp2*xi*Wp1*Ek^2*lr^3*ee+2*w3^2*Wp2*xi*Wp1*Ek^(5/2)*lr^3*li-6*Wp1*Wp2*Ek*lr*w3^(11/4)*Wp3^2*ee-6*Wp1*Wp2*Ek^(3/2)*lr*w3^(3/2)*Wp3^2*li-6*Wp1*Wp2*Ek*lr*w3^(15/4)*Wp3*ee^2-6*Wp1*Wp2*Ek^2*lr*w3^(5/4)*Wp3*li^2-6*Wp1*Wp2*Ek^(3/2)*lr*w3^(7/2)*ee^2*li-6*Wp1*Wp2*Ek^2*lr*w3^(9/4)*ee*li^2+6*w3^(15/4)*Wp1*Wp2*Ek*lr*Wp3^2*ee+6*w3^(5/2)*Wp1*Wp2*Ek^(3/2)*lr*Wp3^2*li+6*w3^(19/4)*Wp1*Wp2*Ek*lr*Wp3*ee^2+6*w3^(9/4)*Wp1*Wp2*Ek^2*lr*Wp3*li^2+6*w3^(9/2)*Wp1*Wp2*Ek^(3/2)*lr*ee^2*li+6*w3^(13/4)*Wp1*Wp2*Ek^2*lr*ee*li^2+2*Wp2^2*xi^2*w3^(7/2)*Wp1^2*Ek^(1/2)*Wp3*ee+2*Wp2^2*xi^2*w3^(9/4)*Wp1^2*Ek*Wp3*li+2*Wp2^2*xi^2*w3^(13/4)*Wp1^2*Ek*ee*li+6*Wp2^2*xi*w3^(9/2)*eta2*Ek^(1/2)*Wp3^2*ee+6*Wp2^2*xi*w3^(13/4)*eta2*Ek*Wp3^2*li+...
                6*Wp2^2*xi*w3^(11/2)*eta2*Ek^(1/2)*Wp3*ee^2+6*Wp2^2*xi*w3^3*eta2*Ek^(3/2)*Wp3*li^2+6*Wp2^2*xi*w3^(21/4)*eta2*Ek*ee^2*li+6*Wp2^2*xi*w3^4*eta2*Ek^(3/2)*ee*li^2+2*Wp2*xi*Wp1*Ek*lr*w3^(7/4)*Wp3^3+2*Wp2*xi*Wp1*Ek*lr*w3^(19/4)*ee^3+2*Wp2*xi*Wp1*Ek^(5/2)*lr*w3*li^3-4*w3^3*eta2*Wp2^2*xi^2*Ek^(3/2)*lr^2*Wp3-4*w3^4*eta2*Wp2^2*xi^2*Ek^(3/2)*lr^2*ee-4*w3^(11/4)*eta2*Wp2^2*xi^2*Ek^2*lr^2*li-2*w3^(13/4)*Wp2*xi*Wp1*Ek^2*lr^3*Wp3-2*w3^(17/4)*Wp2*xi*Wp1*Ek^2*lr^3*ee-2*w3^3*Wp2*xi*Wp1*Ek^(5/2)*lr^3*li-2*w3^(11/4)*Wp2*xi*Wp1*Ek*lr*Wp3^3-2*w3^(23/4)*Wp2*xi*Wp1*Ek*lr*ee^3-2*w3^2*Wp2*xi*Wp1*Ek^(5/2)*lr*li^3+4*w3^4*eta2*Wp2^2*xi^2*Ek^(3/2)*lr^2*Wp3+4*w3^5*eta2*Wp2^2*xi^2*Ek^(3/2)*lr^2*ee+4*w3^(15/4)*eta2*Wp2^2*xi^2*Ek^2*lr^2*li-2*eta2^2*w3^(9/2)*Wp2^2*xi^2*Ek^(1/2)*Wp3*ee-2*eta2^2*w3^(13/4)*Wp2^2*xi^2*Ek*Wp3*li-2*eta2^2*w3^(17/4)*Wp2^2*xi^2*Ek*ee*li+2*eta2^2*w3^(11/2)*Wp2^2*xi^2*Ek^(1/2)*Wp3*ee+2*eta2^2*w3^(17/4)*Wp2^2*xi^2*Ek*Wp3*li+2*eta2^2*w3^(21/4)*Wp2^2*xi^2*Ek*ee*li-2*lr^6*w3^3*Ek^(7/2)+w3^5*eta2^2*Wp2^2*xi^2*Ek^(3/2)*lr^2+Wp2^2*xi^2*w3^3*Wp1^2*Ek^(3/2)*lr^2-12*eta2*w3^5*Ek^(3/2)*lr^2*Wp3*ee^2-12*eta2*w3^(5/2)*Ek^(5/2)*lr^2*Wp3*li^2-12*eta2*w3^(19/4)*Ek^2*lr^2*ee^2*li-12*eta2*w3^(7/2)*Ek^(5/2)*lr^2*ee*li^2-2*Wp2^2*xi*Ek^(3/2)*lr^2*w3^2*Wp3^2-2*Wp2^2*xi*Ek^(3/2)*lr^2*w3^4*ee^2-2*Wp2^2*xi*Ek^(5/2)*lr^2*w3^(3/2)*li^2-2*w3^2*Wp1*Wp2*Ek^(5/2)*lr^3*li+6*eta2*w3^5*Ek^(3/2)*lr^2*Wp3^2*ee+...
                6*eta2*w3^(15/4)*Ek^2*lr^2*Wp3^2*li+6*eta2*w3^6*Ek^(3/2)*lr^2*Wp3*ee^2+6*eta2*w3^(7/2)*Ek^(5/2)*lr^2*Wp3*li^2+6*eta2*w3^(23/4)*Ek^2*lr^2*ee^2*li+6*eta2*w3^(9/2)*Ek^(5/2)*lr^2*ee*li^2+2*Wp2^2*xi*w3^3*Ek^(3/2)*lr^2*Wp3^2+2*Wp2^2*xi*w3^5*Ek^(3/2)*lr^2*ee^2+2*Wp2^2*xi*w3^(5/2)*Ek^(5/2)*lr^2*li^2-2*Wp1*Wp2*Ek*lr*w3^(7/4)*Wp3^3+2*w3^3*Wp1*Wp2*Ek^(5/2)*lr^3*li+2*w3^2*Wp1*Wp2*Ek^(5/2)*lr*li^3-2*Wp2^2*Ek^(3/2)*lr^2*w3^3*Wp3*ee-2*Wp2^2*Ek^2*lr^2*w3^(7/4)*Wp3*li-2*Wp2^2*Ek^2*lr^2*w3^(11/4)*ee*li+12*Wp2^2*w3^(13/4)*Ek*Wp3^2*ee*li+12*Ek^2*lr^2*Wp3^2*w3^(7/4)*ee*li+12*Ek^2*lr^2*Wp3*w3^(11/4)*ee^2*li+12*Ek^(5/2)*lr^2*Wp3*w3^(3/2)*ee*li^2+2*eta2^2*w3^4*Ek^(3/2)*lr^2*Wp3*ee+2*eta2^2*w3^(11/4)*Ek^2*lr^2*Wp3*li+2*eta2^2*w3^(15/4)*Ek^2*lr^2*ee*li-4*eta2^2*w3^5*Ek^(3/2)*lr^2*Wp3*ee-4*eta2^2*w3^(15/4)*Ek^2*lr^2*Wp3*li-4*eta2^2*w3^(19/4)*Ek^2*lr^2*ee*li+2*eta2^2*w3^6*Ek^(3/2)*lr^2*Wp3*ee+2*eta2^2*w3^(19/4)*Ek^2*lr^2*Wp3*li+2*eta2^2*w3^(23/4)*Ek^2*lr^2*ee*li-12*Wp2^2*w3^(9/4)*Ek*Wp3^2*ee*li+Wp2^4*xi^2*w3^(5/2)*Ek^(1/2)*Wp3^2+Wp2^4*xi^2*w3^(9/2)*Ek^(1/2)*ee^2+Wp2^4*xi^2*w3^2*Ek^(3/2)*li^2+2*w3^4*Wp1^2*Ek^(3/2)*lr^2*Wp3*ee+2*w3^(11/4)*Wp1^2*Ek^2*lr^2*Wp3*li+2*w3^(15/4)*Wp1^2*Ek^2*lr^2*ee*li+2*w3^4*Wp2^2*Ek^(3/2)*lr^2*Wp3*ee+2*w3^(11/4)*Wp2^2*Ek^2*lr^2*Wp3*li+2*w3^(15/4)*Wp2^2*Ek^2*lr^2*ee*li+6*eta2*Ek^(3/2)*lr^2*w3^3*Wp3^2*ee+6*eta2*Ek^2*lr^2*w3^(7/4)*Wp3^2*li+6*eta2*Ek^(3/2)*lr^2*w3^4*Wp3*ee^2+...
                6*eta2*Ek^(5/2)*lr^2*w3^(3/2)*Wp3*li^2+6*eta2*Ek^2*lr^2*w3^(15/4)*ee^2*li+6*eta2*Ek^(5/2)*lr^2*w3^(5/2)*ee*li^2-12*eta2*w3^4*Ek^(3/2)*lr^2*Wp3^2*ee-12*eta2*w3^(11/4)*Ek^2*lr^2*Wp3^2*li-12*Wp2^2*xi*w3^(13/4)*eta2*Ek*Wp3*ee*li-12*Wp1*Wp2*Ek^(3/2)*lr*w3^(5/2)*Wp3*ee*li+12*w3^(7/2)*Wp1*Wp2*Ek^(3/2)*lr*Wp3*ee*li+12*Wp2^2*xi*w3^(17/4)*eta2*Ek*Wp3*ee*li+6*Wp2*xi*Wp1*Ek*lr*w3^(11/4)*Wp3^2*ee+6*Wp2*xi*Wp1*Ek^(3/2)*lr*w3^(3/2)*Wp3^2*li+6*Wp2*xi*Wp1*Ek*lr*w3^(15/4)*Wp3*ee^2+12*Wp2*xi*Wp1*Ek^(3/2)*lr*w3^(5/2)*Wp3*ee*li+6*Wp2*xi*Wp1*Ek^2*lr*w3^(5/4)*Wp3*li^2+6*Wp2*xi*Wp1*Ek^(3/2)*lr*w3^(7/2)*ee^2*li+6*Wp2*xi*Wp1*Ek^2*lr*w3^(9/4)*ee*li^2-6*w3^(15/4)*Wp2*xi*Wp1*Ek*lr*Wp3^2*ee-6*w3^(5/2)*Wp2*xi*Wp1*Ek^(3/2)*lr*Wp3^2*li-6*w3^(19/4)*Wp2*xi*Wp1*Ek*lr*Wp3*ee^2-12*w3^(7/2)*Wp2*xi*Wp1*Ek^(3/2)*lr*Wp3*ee*li-6*w3^(9/4)*Wp2*xi*Wp1*Ek^2*lr*Wp3*li^2-6*w3^(9/2)*Wp2*xi*Wp1*Ek^(3/2)*lr*ee^2*li-6*w3^(13/4)*Wp2*xi*Wp1*Ek^2*lr*ee*li^2)*w3^(1/2));
            
            if(length(SupArg)==1), [A,B,C]=Flipper(a,b,c,abs(Ek),Wpx,Wpy,Wpz,1,SupArg(1));
            elseif(length(SupArg)==2), [A,B,C]=Flipper(a,b,c,abs(Ek),Wpx,Wpy,Wpz,1,SupArg(1),SupArg(2));
            elseif(length(SupArg)==5), [A,B,C]=Flipper(a,b,c,abs(Ek),Wpx,Wpy,Wpz,1,SupArg(1),SupArg(2),SupArg(3:5));
            else [A,B,C]=Flipper(a,b,c,abs(Ek),Wpx,Wpy,Wpz,1);
            end;
            
            guess=[0 C linspace(1e-4,1,20)];
            racine=[0]; % will allow to discard solutions too closed from 0
            options=optimset('Display','off','FunValCheck','on','TolFun',1e-15,'TolX',1e-15,'MaxFunEvals',1e4,'MaxIter',1e4);
            for kh=1:length(guess)
                [x,fval,exitflag] = fzero(eqtot,guess(kh),options);
                if(exitflag==1)
                    flagij=0;
                    for kkh=1:length(racine)
                        if(abs(x-racine(kkh))<=1e-10), flagij=1; end;
                    end
                    if(flagij==0), racine=[racine x]; end;
                end
            end
            C0=racine(2:end);
            ff=-(Wp3.*C0.^(1./4)+ee.*C0.^(5./4)+sqrt(Ek).*L(2))./(sqrt(Ek).*L(1).*sqrt(C0));
            gg=xi*Wp2.*C0.^(3./4)./(sqrt(Ek).*L(1));
            B0=(Wp2.*gg+Wp2.*gg.*ff.^2+eta2.*ff.*C0-eta2.*ff.*C0.^2-eta2.*ff.*gg.^2-L(1).*C0.^(1./4).*Ek.^(1./2)-L(1).*C0.^(1./4).*Ek.^(1./2).*ff.^2+L(1).*C0.^(5./4).*Ek.^(1./2)+L(1).*C0.^(5./4).*Ek.^(1./2).*ff.^2)./(Wp1+Wp1.*ff.^2-Wp2.*ff-Wp2.*ff.^3+eta2.*ff.^2.*gg-eta2.*gg);
            A0=ff.*B0+gg;
            compt=0;
            for khj=1:length(C0);
                if(sqrt(imag(A0(khj)).^2+imag(B0(khj)).^2+imag(C0(khj)).^2)<=1e-13 ...
                        && abs(real(C0(khj)))>=1e-13 && abs(real(C0(khj))-1)>=1e-13 && abs(real(A0(khj)))>=1e-15  && abs(real(B0(khj)))>=1e-15)
                    compteur=compteur+1;
                    if((choice==5)&&(length(C)==1)), vis=5e-16; else vis=1e-13; end; % Bad correction
                    if((abs(A0(khj)^2+B0(khj)^2+C0(khj)^2-C0(khj))<=vis)  ...
                            && abs(-xi*Wp2*C0(khj)+Wp3*B0(khj)+ee*B0(khj)*C0(khj)+sqrt(Ek)*(L(1)*A0(khj)*C0(khj)^(1/4)+L(2)*B0(khj)*C0(khj)^(-1/4)))<=1e-13 ...
                            && abs(-Wp1*B0(khj)+Wp2*A0(khj)+eta2*A0(khj)*B0(khj)-L(1)*C0(khj)^(1/4)*(1-C0(khj))*sqrt(Ek))<=1e-13) % WARNING : Wp*si = sign(Wp)*sqrt(Wp1^2+Wp2^2) <> sqrt(Wp1^2+Wp2^2)
                        compt=compt+1; indice(compt)=khj; flag=1;
                    end
                end
            end
            if(compt>0)
                A=A0(indice); B=B0(indice); C=C0(indice);
                eq1=abs(A.^2+B.^2+C.^2-C);
                eq2=abs(-xi*Wp2.*C+Wp3.*B+ee.*B.*C+sqrt(Ek).*(L(1).*A.*C.^(1./4)+L(2).*B.*C.^(-1./4)));
                eq3=abs(-Wp1.*B+Wp2.*A+eta2.*A.*B-L(1).*C.^(1./4).*(1-C).*sqrt(Ek));
            else
                if(details==1), display(C0); display('WARNING Problem'); end
            end
            
        elseif((choice==3)||(choice==4)||(choice==6))
            if(~(imag(Ek)==0))
                if(~(real(Ek)==0))
                    display('Turbulent damping!! (Sous et al. 2013)');
                else
                    display('Turbulent damping!! (Yoder 1981 ; Bowden 1953)');
                end
            end
            if(details==1), display('Noir & Cebron 2013'); end
            if(details==1), display('Guess with'); end
            if((imag(Ek)==0)||(Lauto==0))
                if((choice==3)||(choice==4))
                    if(length(SupArg)==1), [Aguess,Bguess,Cguess]=Flipper(a,b,c,abs(Ek),Wpx,Wpy,Wpz,1,SupArg(1));
                    elseif(length(SupArg)==2), [Aguess,Bguess,Cguess]=Flipper(a,b,c,abs(Ek),Wpx,Wpy,Wpz,1,SupArg(1:2));
                    elseif(length(SupArg)==5), [Aguess,Bguess,Cguess]=Flipper(a,b,c,abs(Ek),Wpx,Wpy,Wpz,1,SupArg(1:5));
                    elseif(length(SupArg)==6), [Aguess,Bguess,Cguess]=Flipper(a,b,c,abs(Ek),Wpx,Wpy,Wpz,1,SupArg(1:6));
                    elseif(length(SupArg)==10), [Aguess,Bguess,Cguess]=Flipper(a,b,c,abs(Ek),Wpx,Wpy,Wpz,1,SupArg(1:10));
                    else [Aguess,Bguess,Cguess]=Flipper(a,b,c,abs(Ek),Wpx,Wpy,Wpz,1);
                    end
                elseif(choice==6)
                    if(length(SupArg)==1), [Aguess,Bguess,Cguess]=Flipper(a,b,c,abs(Ek),Wpx,Wpy,Wpz,1,SupArg(1));
                    elseif(length(SupArg)==2), [Aguess,Bguess,Cguess]=Flipper(a,b,c,abs(Ek),Wpx,Wpy,Wpz,1,SupArg(1:2));
                    elseif(length(SupArg)==5), [Aguess,Bguess,Cguess]=Flipper(a,b,c,abs(Ek),Wpx,Wpy,Wpz,1,SupArg(1:5));
                    elseif(length(SupArg)==6), [Aguess,Bguess,Cguess]=Flipper(a,b,c,abs(Ek),Wpx,Wpy,Wpz,1,SupArg(1:6));
                    elseif(length(SupArg)==10), [Aguess,Bguess,Cguess]=Flipper(a,b,c,abs(Ek),Wpx,Wpy,Wpz,1,SupArg(1:10));
                    else [Aguess,Bguess,Cguess]=Flipper(a,b,c,abs(Ek),Wpx,Wpy,Wpz,1);
                    end
                end
            else
                if(length(SupArg)==10), [Aguess,Bguess,Cguess]=Flipper(a,b,c,abs(Ek),Wpx,Wpy,Wpz,1,SupArg(1:10));
                else
                    [Aguess,Bguess,Cguess]=Flipper(a,b,c,abs(Ek),Wpx,Wpy,Wpz,1,details,0,L);
                end
            end
            
            racine=[0]; racine1=[0]; racine2=[0];  % will allow to discard solutions too closed from 0
            ee=(c^2-a^2)/(c^2+a^2);
            
            options=optimset('Display','off','FunValCheck','on','TolFun',1e-14,'TolX',1e-14,'MaxFunEvals',1e4,'MaxIter',1e4);
            options2=optimset('Algorithm','levenberg-marquardt','Display','off','FunValCheck','on','TolFun',1e-14,'TolX',1e-14,'MaxFunEvals',1e4,'MaxIter',1e4);
            options3=optimset('Algorithm','trust-region-reflective','Display','off','FunValCheck','on','TolFun',1e-14,'TolX',1e-14,'MaxFunEvals',1e4,'MaxIter',1e4);
            
            for khh=1:2
                for kh=1:length(Aguess)
                    if((choice==3)||(choice==4))
                        if(khh==1)
                            [x,fval,exitflag] = fsolve(@(W) FullPrecframeSpheroid(a,b,c,Els,Bmag,W,ee,Ek,Wp,alpha,L(1),L(2),L(3),choice,rICB),[Aguess(kh) Bguess(kh) Cguess(kh)],options);
                        elseif(khh==2)
                            if(choice==3)
                                [x,fval,exitflag] = fsolve(@(W) FullPrecframeSpheroid(a,b,c,Els,Bmag,W,ee,Ek,Wp,alpha,L(1),L(2),L(3),choice,rICB),[Aguess(kh) Bguess(kh) Cguess(kh)],options2);
                            else
                                [x,fval,exitflag] = fsolve(@(W) FullPrecframeSpheroid(a,b,c,Els,Bmag,W,ee,Ek,Wp,alpha,L(1),L(2),L(3),choice,rICB),[Aguess(kh) Bguess(kh) Cguess(kh)],options2);
                            end
                        end
                    elseif(choice==6)
                        if(khh==1)
                            [x,fval,exitflag] = fsolve(@(W) GeneralCebron(Els,Bmag,W,a,b,c,Ek,Wpx,Wpy,Wpz,L,rICB),[Aguess(kh) Bguess(kh) Cguess(kh)],options);
                        elseif(khh==2)
                            [x,fval,exitflag] = fsolve(@(W) GeneralCebron(Els,Bmag,W,a,b,c,Ek,Wpx,Wpy,Wpz,L,rICB),[Aguess(kh) Bguess(kh) Cguess(kh)],options);
                        end
                    end
                    if(exitflag==1)
                        flagij=0;
                        for kkh=1:length(racine)
                            if(abs(x(3)-racine(kkh))<=1e-9), flagij=1; end;
                        end
                        if(flagij==0), racine=[racine x(3)]; racine1=[racine1 x(1)]; racine2=[racine2 x(2)]; end;
                    end
                end
            end
            A0=racine1(2:end); B0=racine2(2:end); C0=racine(2:end);
            
            compt=0;
            for khj=1:length(C0);
                if(sqrt(imag(A0(khj)).^2+imag(B0(khj)).^2+imag(C0(khj)).^2)<=1e-13 ...
                        && abs(real(C0(khj)))>=1e-13 && abs(real(C0(khj))-1)>=1e-13)
                    compt=compt+1; indice(compt)=khj; flag=1; A0(khj)=real(A0(khj));
                    B0(khj)=real(B0(khj)); C0(khj)=real(C0(khj));
                end
            end
            if(compt>0)
                compteur=length(indice);
                if((choice==3)||(choice==4))
                    A=A0(indice).*cos(phi)-B0(indice).*sin(phi); B=A0(indice).*sin(phi)+B0(indice).*cos(phi); C=C0(indice);
                elseif(choice==6)
                    A=A0; B=B0; C=C0;
                end
                eq1=fval;
                eq2=fval;
                eq3=fval;
            else
                if(details==1), display(C0); display('WARNING Problem'); end;
            end
            
        end
        if(flag==0)
            Wp=1.001*Wp;
        end
    end
    
else
    if(~(imag(Ek)==0))
        if(~(real(Ek)==0))
            display('Turbulent damping!! (Sous et al. 2013)');
        else
            display('Turbulent damping!! (Yoder 1981 ; Bowden 1953)');
        end
    end
    X0=1e-10*[1 1 1];
    if(imag(Ek)==0)
        coef=15;
    else
        coef=40;
    end
    if(L(3)==0), T=[0 coef/sqrt(abs(Ek))];
    else T=[0 coef/(abs(L(3))*sqrt(abs(Ek)))]; end;
    if(length(varargin)>=7), T=varargin{7}; end
    if(length(varargin)>=8),
        if(choice<10)
            X00=varargin{8};
            X0(1)=X00(1)*cos(phi)+X00(2)*sin(phi); X0(2)=-X00(1)*sin(phi)+X00(2)*cos(phi); X0(3)=X00(3);
        elseif((choice==10)||(choice==11)||(choice==12))
            X0=varargin{8};
        end
    end
    options = odeset('RelTol',1e-10,'AbsTol',1e-10*[1 1 1]);
    [T,Y] = ode45(@(t,y) triaxPrecFrame_eq(t,y,a,b,c,Ek,Wpx,Wpy,Wpz,Wp,alpha,L,Els,Bmag,choice,rICB),T,X0,options);
    varargout(1)={T}; compteur=1; eq1=0; eq2=0; eq3=0;
    if(choice<10)
        A=Y(:,1)*cos(phi)-Y(:,2)*sin(phi); B=Y(:,1)*sin(phi)+Y(:,2)*cos(phi); C=Y(:,3);
    elseif((choice==10)||(choice==11)||(choice==12))
        A=Y(:,1); B=Y(:,2); C=Y(:,3);
    end
end

theta_t=sign(Wp).*atan(c.^2.*(a.^2+b.^2).*(B.^2.*a.^4.*b.^4+2.*B.^2.*a.^4.*c.^2.*b.^2+a.^4.*c.^4.*B.^2+A.^2.*b.^4.*a.^4+2.*A.^2.*b.^4.*c.^2.*a.^2+A.^2.*b.^4.*c.^4).^(1./2)./(a.^2+c.^2)./b.^2./C./a.^2./(b.^2+c.^2));
phi_t=atan2(-B.*a.^2.*(b.^2+c.^2),A.*b.^2.*(a.^2+c.^2)); D=0;
[aa,bb,bet,bet2,tanThet,xM0,yM0,zM0,X40,Y40,Z40]=ShearElli_eq(a,b,c,sin(theta_t).*cos(phi_t),sin(theta_t).*sin(phi_t),cos(theta_t),D);
eps_t=(1+sign(eta).*bet).*tanThet./2; beta_t=bet;
eps=sign(Wpz).*eps_t;  beta=sign(eta).*beta_t; theta2D=sign(eta).*theta_t;
phi2D=mod(abs(phi_t),pi); NormCenter=sign(Wpz).*atan(tanThet);


%---- Solutions stability : eigenvalues -----------------------------------
if((choice==0)||(choice==1)||(choice==3)||(choice==4))
    %     if(choice==1)
    %         display('Careful! Busse(1968) stability estimated with ODES of Noir & Cebron (2013)!');
    %         display('Differences on equilibrium solutions may strongly affect this estimate!');
    %     end
    lz=[];  Et=[];       A0=A.*cos(phi)+B.*sin(phi); B0=-A.*sin(phi)+B.*cos(phi); C0=C;
    for kg=1:length(A)
        if(choice==0)
            M=[[Ek^(1/2)*L(1),-epsil*Wp*co-epsil*C0(kg)+Wp*co-Ek^(1/2)*L(2),-epsil*B0(kg)];[epsil*Wp*co+epsil*C0(kg)-Wp*co+Ek^(1/2)*L(2),Ek^(1/2)*L(1),Wp*si+epsil*A0(kg)];[0,-Wp*si-epsil*Wp*si,Ek^(1/2)*L(1)]];
            Wx0=A0(kg); Wy0=B0(kg); Wz0=C0(kg); I=complex(0,1);  lr=L(1); li=L(2); E=Ek; e=epsil; Po=Wp;
            %             Et0=[-1/18*(-12*3^(1/6)*e*Po^3*sin(alpha)*Wy0*E^(7/2)*li^7-6*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*E^(3/2)*li^3*cos(alpha)-96*3^(1/6)*e*Po^7*cos(alpha)^4*sin(alpha)*Wy0*E^(3/2)*li^3-3*3^(1/6)*e*Po^9*sin(alpha)*Wy0*E^(1/2)*li-12*3^(1/6)*e*Po^7*sin(alpha)*Wy0*E^(3/2)*li^3-52*3^(2/3)*e*Po^4*cos(alpha)^4*E^2*li^4*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)-15*3^(2/3)*e*Po^2*cos(alpha)^2*E^3*li^6*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)+24*3^(2/3)*e*Po^5*cos(alpha)^5*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*E^(3/2)*li^3+42*3^(2/3)*e*Po^3*cos(alpha)^3*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*E^(5/2)*li^5-36*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^6*cos(alpha)^4*E*li^2-21*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*cos(alpha)^2*E^2*li^4+52*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*cos(alpha)^3*E^(3/2)*li^3+8*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^6*E*li^2+36*E*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*Po^5*cos(alpha)*li+2*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^8-2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(5/2)*li^5*Po*cos(alpha)+90*3^(1/6)*e*Po^6*cos(alpha)*E^2*li^4*sin(alpha)*Wy0+240*3^(1/6)*e*Po^6*cos(alpha)^3*E^2*li^4*sin(alpha)*Wy0+36*3^(1/6)*e*E*li^2*Po^8*sin(alpha)*Wy0*cos(alpha)-72*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^5*sin(alpha)*Wy0*cos(alpha)^2*E^(3/2)*li^3+24*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(3/2)*li^3*Po^3*cos(alpha)+12*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^5*cos(alpha)*E^(1/2)*li-24*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^5*cos(alpha)^3*E*li^2-30*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^3*cos(alpha)*E^2*li^4+48*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^4*cos(alpha)^2*E^(3/2)*li^3-18*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^7*sin(alpha)*Wy0*E^(1/2)*li*cos(alpha)^2+21*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^2*sin(alpha)*Wy0*cos(alpha)*E^3*li^6-6*E^(1/2)*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*Po^6+3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^7*sin(alpha)*Wx0+3*3^(1/6)*e*Po^10*sin(alpha)*Wy0*cos(alpha)+2*3^(2/3)*e*E^(7/2)*li^7*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0+48*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*cos(alpha)^2*E^3*li^6+32*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*cos(alpha)^4*E^2*li^4-64*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^3*cos(alpha)^3*E^(5/2)*li^5-16*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*E^(7/2)*li^7*Po*cos(alpha)-16*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^7*cos(alpha)*E^(1/2)*li+10*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^2*li^4*Wz0*Po*cos(alpha)-2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E*li^2*Po^4-2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(5/2)*li^5*Wz0+2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^5*cos(alpha)*E^(1/2)*li+96*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*cos(alpha)^2*E^2*li^4-48*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^3*E^(5/2)*li^5*cos(alpha)-64*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*cos(alpha)^3*E^(3/2)*li^3+3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*E^3*li^6-2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^7*Wz0*cos(alpha)+3*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^6*E*li^2-48*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*E^(3/2)*li^3*cos(alpha)+48*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^6*cos(alpha)^2*E*li^2+3*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*E^2*li^4+36*E^3*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*li^5*Po*cos(alpha)+8*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^3*cos(alpha)^3*E*li^2*Wz0+2*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*E^4*li^8+3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^8+45*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^4*sin(alpha)*Wy0*cos(alpha)*E^2*li^4-24*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^2*cos(alpha)^2*E^2*li^4+12*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(5/2)*li^5*Po*cos(alpha)+16*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^3*cos(alpha)^3*E^(3/2)*li^3-72*E^(5/2)*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*Po^2*cos(alpha)^2*li^4+3*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^8*sin(alpha)*Wy0*cos(alpha)+12*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*cos(alpha)^4*E*li^2+11*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^2*cos(alpha)^2*E^2*li^4+3*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^6*cos(alpha)^2-3*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^7*sin(alpha)*Wy0*E^(1/2)*li-2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E*li^2*Po^3*sin(alpha)*Wx0-4*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^3*cos(alpha)^2*E*li^2*sin(alpha)*Wx0-6*E^(7/2)*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*li^6+4*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(3/2)*li^3*Po^2*sin(alpha)*Wx0*cos(alpha)-9*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^3*sin(alpha)*Wy0*E^(5/2)*li^5-216*3^(1/6)*e*Po^5*sin(alpha)*Wy0*E^(5/2)*li^5*cos(alpha)^2-144*3^(1/6)*e*Po^7*cos(alpha)^2*sin(alpha)*Wy0*E^(3/2)*li^3-24*3^(1/6)*e*Po^9*sin(alpha)*Wy0*E^(1/2)*li*cos(alpha)^2-54*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^3*sin(alpha)*Wy0*cos(alpha)^2*E^(5/2)*li^5+8*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*E^3*li^6+12*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*E^2*li^4+2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*E^(1/2)*li*Po^6-3*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^8*cos(alpha)^2+3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po*sin(alpha)*Wx0*E^3*li^6-14*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po*cos(alpha)*E^3*li^6+168*3^(1/6)*e*Po^4*cos(alpha)^3*E^3*li^6*sin(alpha)*Wy0+48*3^(1/6)*e*Po^6*cos(alpha)^5*E^2*li^4*sin(alpha)*Wy0-40*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^3*cos(alpha)^3*E^2*li^4+36*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^2*cos(alpha)^2*E^(5/2)*li^5+16*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^4*cos(alpha)^4*E^(3/2)*li^3+6*3^(2/3)*e*E^(5/2)*li^5*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*Wz0+2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^5*Wz0*cos(alpha)+84*3^(1/6)*e*Po^4*cos(alpha)*E^3*li^6*sin(alpha)*Wy0-12*3^(2/3)*e*E^(3/2)*li^3*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*sin(alpha)*Wx0*cos(alpha)-6*3^(2/3)*e*Po^6*sin(alpha)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wx0*cos(alpha)*E^(1/2)*li-12*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^5*cos(alpha)^3*E^(1/2)*li-4*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(3/2)*li^3*Po^2*Wz0-20*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^3*cos(alpha)^3*E^(3/2)*li^3-8*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*cos(alpha)^2*Wz0*E^(1/2)*li-16*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^2*cos(alpha)^2*E^(3/2)*li^3*Wz0-24*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^5*sin(alpha)*Wy0*cos(alpha)^4*E^(3/2)*li^3-4*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^7*cos(alpha)*E^(1/2)*li-18*3^(1/6)*e*Po^5*sin(alpha)*Wy0*E^(5/2)*li^5-3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^5*sin(alpha)*Wx0-3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^2*li^4*Po^2+60*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^4*sin(alpha)*Wy0*cos(alpha)^3*E^2*li^4+12*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E*li^2*Po^3*Wz0*cos(alpha)-3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^6-9*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^5*sin(alpha)*Wy0*E^(3/2)*li^3+12*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^6*cos(alpha)^2*E^(1/2)*li-18*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^5*cos(alpha)*E*li^2-6*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E*li^2*Po^4-6*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^2*li^4*Po^2+10*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*cos(alpha)^2*E*li^2-3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^2*li^4*Po*sin(alpha)*Wx0+18*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^7*cos(alpha)^3*E^(1/2)*li+6*3^(2/3)*e*E^(3/2)*li^3*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*Wz0-9*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^6*E*li^2*cos(alpha)^2+2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po*cos(alpha)*E^(7/2)*li^7-3*3^(1/6)*e*E^(9/2)*li^9*Po*sin(alpha)*Wy0-3*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po*sin(alpha)*Wy0*E^(7/2)*li^7-2*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^6+72*E^2*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*li^3*Po^3*cos(alpha)+4*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*sin(alpha)*Wx0*cos(alpha)*E^(1/2)*li+27*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^6*sin(alpha)*Wy0*cos(alpha)*E*li^2-24*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*cos(alpha)^2*E*li^2+27*3^(1/6)*e*Po^2*cos(alpha)*E^4*li^8*sin(alpha)*Wy0+12*3^(2/3)*e*Po^3*cos(alpha)^2*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*E^2*li^4*sin(alpha)*Wx0-8*3^(2/3)*e*Po^4*cos(alpha)^3*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*E^(3/2)*li^3*sin(alpha)*Wx0-6*3^(2/3)*e*E^(5/2)*li^5*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*sin(alpha)*Wx0*cos(alpha)-144*3^(1/6)*e*Po^5*cos(alpha)^4*sin(alpha)*Wy0*E^(5/2)*li^5-96*3^(1/6)*e*Po^3*cos(alpha)^2*sin(alpha)*Wy0*E^(7/2)*li^7+3*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^3*sin(alpha)*Wx0*E^2*li^4+3*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*sin(alpha)*Wx0*E*li^2+12*3^(2/3)*e*Po^5*cos(alpha)^2*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*E*li^2*sin(alpha)*Wx0+72*3^(1/6)*e*Po^8*cos(alpha)^3*E*li^2*sin(alpha)*Wy0-18*E^(5/2)*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*li^4*Po^2-18*E^(3/2)*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*li^2*Po^4-72*E^(3/2)*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*Po^4*cos(alpha)^2*li^2-2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*Wz0*E^(1/2)*li+36*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^6*sin(alpha)*Wy0*cos(alpha)^3*E*li^2-2*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^3*li^6+48*E^2*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*Po^3*cos(alpha)^3*li^3)*3^(5/6)/((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)/(E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3 1/36*(12*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^5*cos(alpha)*E^(1/2)*li+24*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(3/2)*li^3*Po^3*cos(alpha)-9*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^6*cos(alpha)^2+12*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*E^2*li^4+8*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*E^3*li^6+8*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^6*E*li^2-3*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^8*cos(alpha)^2+18*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^7*sin(alpha)*Wy0*E^(1/2)*li*cos(alpha)^2+72*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^5*sin(alpha)*Wy0*E^(3/2)*li^3*cos(alpha)^2-24*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*cos(alpha)^2*E*li^2-30*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^2*li^4*Wz0*Po*cos(alpha)+10*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^2*li^4*Wz0*Po*cos(alpha)+10*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*cos(alpha)^2*E*li^2-12*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^5*cos(alpha)^3*E^(1/2)*li+27*3^(1/6)*e*E^4*li^8*Po^2*sin(alpha)*Wy0*cos(alpha)+12*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E*li^2*Po^3*Wz0*cos(alpha)-18*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^7*sin(alpha)*Wy0*E^(1/2)*li*cos(alpha)^2-2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E*li^2*Po^4+6*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(5/2)*li^5*Wz0+12*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^3*cos(alpha)^2*E*li^2*sin(alpha)*Wx0-8*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*cos(alpha)^2*Wz0*E^(1/2)*li+45*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^4*sin(alpha)*Wy0*cos(alpha)*E^2*li^4-24*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^3*cos(alpha)^3*E*li^2*Wz0+36*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^5*cos(alpha)^3*E^(1/2)*li+48*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^2*cos(alpha)^2*E^(3/2)*li^3*Wz0-36*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^6*sin(alpha)*Wy0*cos(alpha)^3*E*li^2+9*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^5*sin(alpha)*Wy0*E^(3/2)*li^3+12*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*sin(alpha)*Wx0*cos(alpha)^2*E*li^2-12*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*sin(alpha)*Wx0*E^(3/2)*li^3*cos(alpha)-6*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^6*sin(alpha)*Wx0*cos(alpha)*E^(1/2)*li+3*3^(2/3)*e*E^2*li^4*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^3*sin(alpha)*Wx0-96*3^(1/6)*e*E^(3/2)*li^3*Po^7*sin(alpha)*Wy0*cos(alpha)^4-144*3^(1/6)*e*E^(3/2)*li^3*Po^7*sin(alpha)*Wy0*cos(alpha)^2-216*3^(1/6)*e*E^(5/2)*li^5*Po^5*sin(alpha)*Wy0*cos(alpha)^2+3*3^(2/3)*e*E*li^2*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*sin(alpha)*Wx0-30*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^3*cos(alpha)*E^2*li^4-24*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^5*cos(alpha)^3*E*li^2-33*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^2*cos(alpha)^2*E^2*li^4-36*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E*li^2*Po^3*Wz0*cos(alpha)+2*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^8+3*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^6+6*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^3*li^6-2*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^3*li^6-54*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^3*sin(alpha)*Wy0*cos(alpha)^2*E^(5/2)*li^5-3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^6+8*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^3*cos(alpha)^3*E*li^2*Wz0+3^(2/3)*e*E^3*li^6*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po*sin(alpha)*Wx0-14*3^(2/3)*e*E^3*li^6*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po*cos(alpha)-96*3^(1/6)*e*E^(7/2)*li^7*Po^3*sin(alpha)*Wy0*cos(alpha)^2-144*3^(1/6)*e*E^(5/2)*li^5*Po^5*sin(alpha)*Wy0*cos(alpha)^4-36*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*cos(alpha)^4*E*li^2+2*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*E^4*li^8+3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^8+6*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^8-24*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^5*sin(alpha)*Wy0*cos(alpha)^4*E^(3/2)*li^3+6*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^6+60*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^4*sin(alpha)*Wy0*cos(alpha)^3*E^2*li^4+2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*E^(1/2)*li*Po^6-4*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^7*cos(alpha)*E^(1/2)*li+3*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^8*sin(alpha)*Wy0*cos(alpha)-6*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^5*cos(alpha)*E^(1/2)*li+6*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E*li^2*Po^3*sin(alpha)*Wx0-21*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^2*sin(alpha)*Wy0*cos(alpha)*E^3*li^6+3*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^5*sin(alpha)*Wx0+18*I*3^(1/6)*e*E^(5/2)*li^5*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*Wz0-3*I*3^(2/3)*e*Po^9*sin(alpha)*Wy0*E^(1/2)*li-24*I*3^(2/3)*e*Po^9*sin(alpha)*Wy0*E^(1/2)*li*cos(alpha)^2+84*I*3^(2/3)*e*E^3*li^6*Po^4*sin(alpha)*Wy0*cos(alpha)+36*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*sin(alpha)*Wx0*cos(alpha)^2*E*li^2-36*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*sin(alpha)*Wx0*E^(3/2)*li^3*cos(alpha)-18*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^6*sin(alpha)*Wx0*cos(alpha)*E^(1/2)*li+90*I*3^(2/3)*e*E^2*li^4*Po^6*sin(alpha)*Wy0*cos(alpha)-16*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^2*cos(alpha)^2*E^(3/2)*li^3*Wz0-12*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(3/2)*li^3*Po^2*sin(alpha)*Wx0*cos(alpha)-2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(5/2)*li^5*Wz0-4*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^3*cos(alpha)^2*E*li^2*sin(alpha)*Wx0-64*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*E^(5/2)*li^5*Po^3*cos(alpha)^3-16*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po*cos(alpha)*E^(7/2)*li^7+3*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^6*E*li^2-16*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^7*cos(alpha)*E^(1/2)*li-2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^7*cos(alpha)+3*3^(1/6)*e*Po^10*sin(alpha)*Wy0*cos(alpha)+3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^7*sin(alpha)*Wx0+48*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^6*cos(alpha)^2*E*li^2-48*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*E^(3/2)*li^3*cos(alpha)+3*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*E^2*li^4+96*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*cos(alpha)^2*E^2*li^4-48*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^3*E^(5/2)*li^5*cos(alpha)-64*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*cos(alpha)^3*E^(3/2)*li^3+3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*E^3*li^6+2*3^(2/3)*e*E^(7/2)*li^7*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0+48*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*cos(alpha)^2*E^3*li^6+32*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*cos(alpha)^4*E^2*li^4+48*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^4*cos(alpha)^2*E^(3/2)*li^3+144*E^(5/2)*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*Po^2*cos(alpha)^2*li^4+18*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E*li^2*Po^4-24*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^2*cos(alpha)^2*E^2*li^4+24*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*E*li^2*Po^6+36*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*E^2*li^4*Po^4-9*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^8*cos(alpha)^2+24*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*E^3*li^6*Po^2+12*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(3/2)*li^3*Po^2*Wz0+54*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^3*sin(alpha)*Wy0*cos(alpha)^2*E^(5/2)*li^5+24*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^5*sin(alpha)*Wy0*cos(alpha)^4*E^(3/2)*li^3+240*3^(1/6)*e*E^2*li^4*Po^6*sin(alpha)*Wy0*cos(alpha)^3-24*3^(1/6)*e*Po^9*cos(alpha)^2*sin(alpha)*Wy0*E^(1/2)*li-36*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^5*cos(alpha)*E^(1/2)*li+3*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^8+6*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*E^4*li^8-6*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^5*Wz0*cos(alpha)-144*E^2*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*li^3*Po^3*cos(alpha)-45*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*cos(alpha)^2*E^3*li^6-156*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*cos(alpha)^4*E^2*li^4+72*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*cos(alpha)^5*E^(3/2)*li^3+36*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^3*sin(alpha)*Wx0*cos(alpha)^2*E^2*li^4-24*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*sin(alpha)*Wx0*cos(alpha)^3*E^(3/2)*li^3+36*I*3^(2/3)*e*E*li^2*Po^8*sin(alpha)*Wy0*cos(alpha)+9*I*3^(1/6)*e*E*li^2*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*sin(alpha)*Wx0+6*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po*cos(alpha)*E^(7/2)*li^7-3*I*3^(2/3)*e*E^(9/2)*li^9*Po*sin(alpha)*Wy0-90*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^3*cos(alpha)*E^2*li^4-120*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*E^2*li^4*Po^3*cos(alpha)^3+108*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*E^(5/2)*li^5*Po^2*cos(alpha)^2+48*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*E^(3/2)*li^3*Po^4*cos(alpha)^4+156*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*cos(alpha)^3*E^(3/2)*li^3+54*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^7*cos(alpha)^3*E^(1/2)*li-108*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^6*cos(alpha)^4*E*li^2-63*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*cos(alpha)^2*E^2*li^4-72*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^5*cos(alpha)^3*E*li^2+144*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^4*cos(alpha)^2*E^(3/2)*li^3+36*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^6*cos(alpha)^2*E^(1/2)*li+168*I*3^(2/3)*e*Po^4*cos(alpha)^3*E^3*li^6*sin(alpha)*Wy0+48*I*3^(2/3)*e*Po^6*cos(alpha)^5*E^2*li^4*sin(alpha)*Wy0-96*I*3^(2/3)*e*Po^3*cos(alpha)^2*sin(alpha)*Wy0*E^(7/2)*li^7-144*I*3^(2/3)*e*Po^5*cos(alpha)^4*sin(alpha)*Wy0*E^(5/2)*li^5-18*I*3^(2/3)*e*E^(5/2)*li^5*Po^5*sin(alpha)*Wy0-216*I*3^(2/3)*e*E^(5/2)*li^5*Po^5*sin(alpha)*Wy0*cos(alpha)^2-12*I*3^(2/3)*e*E^(3/2)*li^3*Po^7*sin(alpha)*Wy0-144*I*3^(2/3)*e*E^(3/2)*li^3*Po^7*sin(alpha)*Wy0*cos(alpha)^2-96*I*3^(2/3)*e*E^(3/2)*li^3*Po^7*sin(alpha)*Wy0*cos(alpha)^4+240*I*3^(2/3)*e*Po^6*cos(alpha)^3*E^2*li^4*sin(alpha)*Wy0+72*I*3^(2/3)*e*Po^8*cos(alpha)^3*E*li^2*sin(alpha)*Wy0-12*I*3^(2/3)*e*Po^3*sin(alpha)*Wy0*E^(7/2)*li^7-27*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^6*E*li^2*cos(alpha)^2+9*I*3^(1/6)*e*E^2*li^4*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^3*sin(alpha)*Wx0+6*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*E^(1/2)*li*Po^6-12*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^7*cos(alpha)*E^(1/2)*li+3*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po*sin(alpha)*Wx0*E^3*li^6+27*I*3^(2/3)*e*E^4*li^8*Po^2*sin(alpha)*Wy0*cos(alpha)+18*I*3^(1/6)*e*E^(3/2)*li^3*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*Wz0-18*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*E^(3/2)*li^3*cos(alpha)-54*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^5*cos(alpha)*E*li^2+126*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^3*cos(alpha)^3*E^(5/2)*li^5-18*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*sin(alpha)*Wx0*E^(5/2)*li^5*cos(alpha)-42*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*E^3*li^6*Po*cos(alpha)+3*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^7*sin(alpha)*Wy0*E^(1/2)*li+4*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(3/2)*li^3*Po^2*sin(alpha)*Wx0*cos(alpha)-60*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^4*sin(alpha)*Wy0*cos(alpha)^3*E^2*li^4+27*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^6*sin(alpha)*Wy0*cos(alpha)*E*li^2-4*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(3/2)*li^3*Po^2*Wz0+6*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*Wz0*E^(1/2)*li+36*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^6*sin(alpha)*Wy0*cos(alpha)^3*E*li^2-30*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*cos(alpha)^2*E*li^2-9*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^5*sin(alpha)*Wy0*E^(3/2)*li^3+3*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^6*cos(alpha)^2-6*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E*li^2*Po^4-6*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^2*li^4*Po^2-3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^2*li^4*Po*sin(alpha)*Wx0-45*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^4*sin(alpha)*Wy0*cos(alpha)*E^2*li^4-27*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^6*sin(alpha)*Wy0*cos(alpha)*E*li^2+4*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*sin(alpha)*Wx0*cos(alpha)*E^(1/2)*li-3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^5*sin(alpha)*Wx0+3*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^2*li^4*Po*sin(alpha)*Wx0+18*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^7*cos(alpha)^3*E^(1/2)*li-6*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*E^(3/2)*li^3*cos(alpha)+6*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*E^(3/2)*li^3*Po^4-3*3^(1/6)*e*E^(9/2)*li^9*Po*sin(alpha)*Wy0+24*3^(2/3)*e*E^(3/2)*li^3*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*cos(alpha)^5+42*3^(2/3)*e*E^(5/2)*li^5*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^3*cos(alpha)^3+2*3^(2/3)*e*E^(7/2)*li^7*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po*cos(alpha)-52*3^(2/3)*e*Po^4*cos(alpha)^4*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*E^2*li^4-15*3^(2/3)*e*E^3*li^6*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*cos(alpha)^2-9*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^6*E*li^2*cos(alpha)^2-18*3^(1/6)*e*Po^5*sin(alpha)*Wy0*E^(5/2)*li^5-12*3^(1/6)*e*Po^7*sin(alpha)*Wy0*E^(3/2)*li^3-3*3^(1/6)*e*Po^9*sin(alpha)*Wy0*E^(1/2)*li-36*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^6*cos(alpha)^4*E*li^2-21*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*cos(alpha)^2*E^2*li^4+52*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*cos(alpha)^3*E^(3/2)*li^3+6*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*E^(5/2)*li^5*Po^2-12*3^(1/6)*e*E^(7/2)*li^7*Po^3*sin(alpha)*Wy0+36*3^(1/6)*e*E*li^2*Po^8*sin(alpha)*Wy0*cos(alpha)+72*3^(1/6)*e*E*li^2*Po^8*sin(alpha)*Wy0*cos(alpha)^3+84*3^(1/6)*e*E^3*li^6*Po^4*sin(alpha)*Wy0*cos(alpha)+90*3^(1/6)*e*E^2*li^4*Po^6*sin(alpha)*Wy0*cos(alpha)+6*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E*li^2*Po^4-12*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*sin(alpha)*Wx0*cos(alpha)*E^(1/2)*li-72*E*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*Po^5*cos(alpha)*li-40*3^(2/3)*e*Po^3*cos(alpha)^3*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*E^2*li^4*Wz0+16*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*E^(3/2)*li^3*Po^4*cos(alpha)^4+36*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*E^(5/2)*li^5*Po^2*cos(alpha)^2+12*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^3*sin(alpha)*Wx0*cos(alpha)^2*E^2*li^4-6*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*sin(alpha)*Wx0*E^(5/2)*li^5*cos(alpha)-8*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*sin(alpha)*Wx0*cos(alpha)^3*E^(3/2)*li^3+168*3^(1/6)*e*Po^4*cos(alpha)^3*sin(alpha)*Wy0*E^3*li^6+48*3^(1/6)*e*Po^6*cos(alpha)^5*sin(alpha)*Wy0*E^2*li^4+12*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^6*cos(alpha)^2*E^(1/2)*li-18*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^5*cos(alpha)*E*li^2-3*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po*sin(alpha)*Wy0*E^(7/2)*li^7+2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^5*Wz0*cos(alpha)+12*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(5/2)*li^5*Po*cos(alpha)+16*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^3*cos(alpha)^3*E^(3/2)*li^3-72*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^5*sin(alpha)*Wy0*cos(alpha)^2*E^(3/2)*li^3-2*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^6+60*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^3*cos(alpha)^3*E^(3/2)*li^3-72*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(3/2)*li^3*Po^3*cos(alpha)-9*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^3*sin(alpha)*Wy0*E^(5/2)*li^5-36*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(5/2)*li^5*Po*cos(alpha)-3*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^7*sin(alpha)*Wy0*E^(1/2)*li+36*E^(3/2)*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*li^2*Po^4+36*E^(5/2)*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*li^4*Po^2+72*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*cos(alpha)^2*E*li^2-20*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^3*cos(alpha)^3*E^(3/2)*li^3-2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(5/2)*li^5*Po*cos(alpha)-2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E*li^2*Po^3*sin(alpha)*Wx0+9*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^3*sin(alpha)*Wy0*E^(5/2)*li^5+21*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^2*sin(alpha)*Wy0*cos(alpha)*E^3*li^6+72*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^2*cos(alpha)^2*E^2*li^4+3*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^2*li^4*Po^2+3*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po*sin(alpha)*Wy0*E^(7/2)*li^7+6*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(5/2)*li^5*Po*cos(alpha)-48*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^3*cos(alpha)^3*E^(3/2)*li^3+12*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*cos(alpha)^4*E*li^2+11*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^2*cos(alpha)^2*E^2*li^4-2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*Wz0*E^(1/2)*li+2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^5*cos(alpha)*E^(1/2)*li-72*E^3*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*li^5*Po*cos(alpha)-96*E^2*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*Po^3*cos(alpha)^3*li^3+24*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*cos(alpha)^2*Wz0*E^(1/2)*li+144*E^(3/2)*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*Po^4*cos(alpha)^2*li^2+12*E^(7/2)*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*li^6+12*E^(1/2)*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*Po^6-3*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^8*sin(alpha)*Wy0*cos(alpha)+18*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^2*li^4*Po^2+3*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^7*sin(alpha)*Wx0-144*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*E^(3/2)*li^3*cos(alpha)+9*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*E^2*li^4+3*I*3^(2/3)*e*Po^10*sin(alpha)*Wy0*cos(alpha)+3*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*E^3*li^6-144*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^3*E^(5/2)*li^5*cos(alpha)-48*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*E^(7/2)*li^7*Po*cos(alpha)+6*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*E^(7/2)*li^7-192*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*cos(alpha)^3*E^(3/2)*li^3+288*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*cos(alpha)^2*E^2*li^4+144*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^6*cos(alpha)^2*E*li^2-192*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^3*cos(alpha)^3*E^(5/2)*li^5+144*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*cos(alpha)^2*E^3*li^6+96*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*cos(alpha)^4*E^2*li^4-48*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^7*cos(alpha)*E^(1/2)*li+9*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^6*E*li^2-6*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^7*cos(alpha)-3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^2*li^4*Po^2)*3^(5/6)/((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)/(E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3 1/36*(16*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^3*cos(alpha)^3*E^(3/2)*li^3+21*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^2*sin(alpha)*Wy0*cos(alpha)*E^3*li^6-24*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^2*cos(alpha)^2*E^2*li^4-3*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^2*li^4*Po^2+36*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^6*sin(alpha)*Wy0*cos(alpha)^3*E*li^2-18*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^7*sin(alpha)*Wy0*E^(1/2)*li*cos(alpha)^2+33*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^2*cos(alpha)^2*E^2*li^4-3*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^7*sin(alpha)*Wy0*E^(1/2)*li+27*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^6*sin(alpha)*Wy0*cos(alpha)*E*li^2-54*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^3*sin(alpha)*Wy0*cos(alpha)^2*E^(5/2)*li^5+3*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^8*sin(alpha)*Wy0*cos(alpha)+24*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^3*cos(alpha)^3*E*li^2*Wz0-144*E^2*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*li^3*Po^3*cos(alpha)+3*3^(2/3)*e*E*li^2*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*sin(alpha)*Wx0-24*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^5*cos(alpha)^3*E*li^2+48*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^4*cos(alpha)^2*E^(3/2)*li^3-30*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^3*cos(alpha)*E^2*li^4+12*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(5/2)*li^5*Po*cos(alpha)+12*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^5*cos(alpha)*E^(1/2)*li-6*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E*li^2*Po^3*sin(alpha)*Wx0+4*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*sin(alpha)*Wx0*cos(alpha)*E^(1/2)*li+2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^5*cos(alpha)*E^(1/2)*li-72*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^5*sin(alpha)*Wy0*E^(3/2)*li^3*cos(alpha)^2+72*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(3/2)*li^3*Po^3*cos(alpha)-9*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^5*sin(alpha)*Wy0*E^(3/2)*li^3-6*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^3*li^6-3*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^6-60*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^3*cos(alpha)^3*E^(3/2)*li^3+60*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^4*sin(alpha)*Wy0*cos(alpha)^3*E^2*li^4-72*E*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*Po^5*cos(alpha)*li+16*3^(2/3)*e*E^(3/2)*li^3*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*cos(alpha)^4*Wz0+36*3^(2/3)*e*E^(5/2)*li^5*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*cos(alpha)^2*Wz0-6*3^(2/3)*e*E^(5/2)*li^5*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*sin(alpha)*Wx0*cos(alpha)-6*3^(2/3)*e*Po^6*cos(alpha)*E^(1/2)*li*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*sin(alpha)*Wx0+12*3^(2/3)*e*E*li^2*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*sin(alpha)*Wx0*cos(alpha)^2-12*3^(2/3)*e*E^(3/2)*li^3*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*sin(alpha)*Wx0*cos(alpha)-24*3^(1/6)*e*Po^9*sin(alpha)*Wy0*E^(1/2)*li*cos(alpha)^2+144*E^(5/2)*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*Po^2*cos(alpha)^2*li^4-9*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^3*sin(alpha)*Wy0*E^(5/2)*li^5-3*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po*sin(alpha)*Wy0*E^(7/2)*li^7-20*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^3*cos(alpha)^3*E^(3/2)*li^3-3*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^8-6*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*E^4*li^8+2*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^8+27*3^(1/6)*e*E^4*li^8*Po^2*sin(alpha)*Wy0*cos(alpha)+48*3^(1/6)*e*E^2*li^4*Po^6*sin(alpha)*Wy0*cos(alpha)^5+168*3^(1/6)*e*E^3*li^6*Po^4*sin(alpha)*Wy0*cos(alpha)^3-96*3^(1/6)*e*Po^3*cos(alpha)^2*sin(alpha)*Wy0*E^(7/2)*li^7-3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^5*sin(alpha)*Wx0-72*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*cos(alpha)^2*E*li^2-6*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^8-36*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*sin(alpha)*Wx0*cos(alpha)^2*E*li^2-36*I*3^(2/3)*e*Po^8*cos(alpha)*E*li^2*sin(alpha)*Wy0-72*I*3^(2/3)*e*Po^8*cos(alpha)^3*E*li^2*sin(alpha)*Wy0-126*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^3*cos(alpha)^3*E^(5/2)*li^5+54*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^5*cos(alpha)*E*li^2-18*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*E^(3/2)*li^3*Po^4+18*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*E^(3/2)*li^3*cos(alpha)+18*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*sin(alpha)*Wx0*E^(5/2)*li^5*cos(alpha)+144*E^(3/2)*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*Po^4*cos(alpha)^2*li^2+12*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*sin(alpha)*Wx0*cos(alpha)*E^(1/2)*li-72*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^5*sin(alpha)*Wy0*E^(3/2)*li^3*cos(alpha)^2+3^(2/3)*e*E^3*li^6*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po*sin(alpha)*Wx0-14*3^(2/3)*e*E^3*li^6*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po*cos(alpha)-40*3^(2/3)*e*E^2*li^4*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^3*cos(alpha)^3*Wz0-144*3^(1/6)*e*Po^5*cos(alpha)^4*E^(5/2)*li^5*sin(alpha)*Wy0+12*3^(2/3)*e*E^2*li^4*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^3*cos(alpha)^2*sin(alpha)*Wx0-8*3^(2/3)*e*E^(3/2)*li^3*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*cos(alpha)^3*sin(alpha)*Wx0+27*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^6*sin(alpha)*Wy0*cos(alpha)*E*li^2+3*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^8*sin(alpha)*Wy0*cos(alpha)-24*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*E*li^2*Po^6-36*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*E^2*li^4*Po^4-24*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*E^3*li^6*Po^2+9*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^8*cos(alpha)^2-6*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*E^(7/2)*li^7+48*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^7*cos(alpha)*E^(1/2)*li+6*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^7*cos(alpha)-3*I*3^(2/3)*e*Po^10*sin(alpha)*Wy0*cos(alpha)+144*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^3*E^(5/2)*li^5*cos(alpha)-3*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*E^3*li^6-3*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^7*sin(alpha)*Wx0+48*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*E^(7/2)*li^7*Po*cos(alpha)-9*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^6*E*li^2-288*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*E^2*li^4*Po^4*cos(alpha)^2-144*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*E*li^2*Po^6*cos(alpha)^2+192*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*cos(alpha)^3*E^(3/2)*li^3+192*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^3*cos(alpha)^3*E^(5/2)*li^5-96*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*cos(alpha)^4*E^2*li^4-144*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*cos(alpha)^2*E^3*li^6+144*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*E^(3/2)*li^3*cos(alpha)-9*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*E^2*li^4+30*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^2*li^4*Wz0*Po*cos(alpha)+2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^5*Wz0*cos(alpha)-2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E*li^2*Po^4+10*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^2*li^4*Wz0*Po*cos(alpha)+36*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E*li^2*Po^3*Wz0*cos(alpha)-4*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^3*cos(alpha)^2*E*li^2*sin(alpha)*Wx0-24*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*cos(alpha)^2*E*li^2-3*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^5*sin(alpha)*Wx0-24*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*cos(alpha)^2*Wz0*E^(1/2)*li-6*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(5/2)*li^5*Wz0-2*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^6-4*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(3/2)*li^3*Po^2*Wz0+36*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(5/2)*li^5*Po*cos(alpha)+4*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(3/2)*li^3*Po^2*sin(alpha)*Wx0*cos(alpha)-2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(5/2)*li^5*Wz0-12*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(3/2)*li^3*Po^2*Wz0+36*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^5*cos(alpha)*E^(1/2)*li+36*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^6*sin(alpha)*Wy0*cos(alpha)^3*E*li^2+3^(2/3)*e*Po^7*sin(alpha)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wx0+48*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*cos(alpha)^2*E^3*li^6+32*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*cos(alpha)^4*E^2*li^4+3*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*E^2*li^4+48*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^6*cos(alpha)^2*E*li^2-48*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*E^(3/2)*li^3*cos(alpha)+24*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(3/2)*li^3*Po^3*cos(alpha)+3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*E^3*li^6+96*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*cos(alpha)^2*E^2*li^4-48*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^3*E^(5/2)*li^5*cos(alpha)-64*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*cos(alpha)^3*E^(3/2)*li^3-16*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^7*cos(alpha)*E^(1/2)*li-2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^7*cos(alpha)+3*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^6*E*li^2+2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*E^(7/2)*li^7+3*3^(1/6)*e*Po^10*sin(alpha)*Wy0*cos(alpha)-16*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po*cos(alpha)*E^(7/2)*li^7-64*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^3*cos(alpha)^3*E^(5/2)*li^5-18*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^2*li^4*Po^2+9*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^6*cos(alpha)^2-2*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^3*li^6-9*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^3*sin(alpha)*Wy0*E^(5/2)*li^5-36*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^5*cos(alpha)^3*E^(1/2)*li-216*3^(1/6)*e*E^(5/2)*li^5*Po^5*sin(alpha)*Wy0*cos(alpha)^2+240*3^(1/6)*e*E^2*li^4*Po^6*sin(alpha)*Wy0*cos(alpha)^3+84*3^(1/6)*e*E^3*li^6*Po^4*sin(alpha)*Wy0*cos(alpha)+90*3^(1/6)*e*E^2*li^4*Po^6*sin(alpha)*Wy0*cos(alpha)-144*3^(1/6)*e*E^(3/2)*li^3*Po^7*sin(alpha)*Wy0*cos(alpha)^2-96*3^(1/6)*e*E^(3/2)*li^3*Po^7*sin(alpha)*Wy0*cos(alpha)^4+36*3^(1/6)*e*E*li^2*Po^8*sin(alpha)*Wy0*cos(alpha)+72*3^(1/6)*e*E*li^2*Po^8*sin(alpha)*Wy0*cos(alpha)^3+3*3^(2/3)*e*E^2*li^4*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^3*sin(alpha)*Wx0-18*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E*li^2*Po^4-24*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^5*sin(alpha)*Wy0*cos(alpha)^4*E^(3/2)*li^3-24*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^5*sin(alpha)*Wy0*cos(alpha)^4*E^(3/2)*li^3+12*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(3/2)*li^3*Po^2*sin(alpha)*Wx0*cos(alpha)-3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^2*li^4*Po^2-3*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^2*li^4*Po*sin(alpha)*Wx0-6*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(5/2)*li^5*Po*cos(alpha)-54*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^3*sin(alpha)*Wy0*cos(alpha)^2*E^(5/2)*li^5-12*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^3*cos(alpha)^2*E*li^2*sin(alpha)*Wx0-3*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^7*sin(alpha)*Wy0*E^(1/2)*li+3*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^6*cos(alpha)^2-6*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^2*li^4*Po^2-6*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E*li^2*Po^4+8*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^6*E*li^2-3*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^8*cos(alpha)^2+12*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*E^2*li^4+8*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*E^3*li^6+12*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^7*cos(alpha)*E^(1/2)*li-6*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*E^(1/2)*li*Po^6+90*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^3*cos(alpha)*E^2*li^4-18*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*E^(5/2)*li^5*Po^2+3*I*3^(2/3)*e*E^(9/2)*li^9*Po*sin(alpha)*Wy0+108*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^6*cos(alpha)^4*E*li^2+63*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*cos(alpha)^2*E^2*li^4-156*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*cos(alpha)^3*E^(3/2)*li^3+96*I*3^(2/3)*e*Po^3*cos(alpha)^2*sin(alpha)*Wy0*E^(7/2)*li^7+144*I*3^(2/3)*e*Po^5*cos(alpha)^4*E^(5/2)*li^5*sin(alpha)*Wy0-48*I*3^(2/3)*e*Po^6*cos(alpha)^5*E^2*li^4*sin(alpha)*Wy0-168*I*3^(2/3)*e*Po^4*cos(alpha)^3*E^3*li^6*sin(alpha)*Wy0+3*I*3^(2/3)*e*Po^9*sin(alpha)*Wy0*E^(1/2)*li-240*I*3^(2/3)*e*Po^6*cos(alpha)^3*E^2*li^4*sin(alpha)*Wy0-84*I*3^(2/3)*e*Po^4*cos(alpha)*E^3*li^6*sin(alpha)*Wy0-90*I*3^(2/3)*e*Po^6*cos(alpha)*E^2*li^4*sin(alpha)*Wy0+120*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^3*cos(alpha)^3*E^2*li^4-108*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^2*cos(alpha)^2*E^(5/2)*li^5-48*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^4*cos(alpha)^4*E^(3/2)*li^3+45*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*cos(alpha)^2*E^3*li^6+156*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*cos(alpha)^4*E^2*li^4-72*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*cos(alpha)^5*E^(3/2)*li^3-36*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^3*sin(alpha)*Wx0*cos(alpha)^2*E^2*li^4+24*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*sin(alpha)*Wx0*cos(alpha)^3*E^(3/2)*li^3-6*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po*cos(alpha)*E^(7/2)*li^7-27*I*3^(2/3)*e*Po^2*cos(alpha)*E^4*li^8*sin(alpha)*Wy0-3*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po*sin(alpha)*Wx0*E^3*li^6+42*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po*cos(alpha)*E^3*li^6+12*I*3^(2/3)*e*E^(3/2)*li^3*Po^7*sin(alpha)*Wy0+144*I*3^(2/3)*e*E^(3/2)*li^3*Po^7*sin(alpha)*Wy0*cos(alpha)^2+96*I*3^(2/3)*e*E^(3/2)*li^3*Po^7*sin(alpha)*Wy0*cos(alpha)^4+18*I*3^(2/3)*e*E^(5/2)*li^5*Po^5*sin(alpha)*Wy0+216*I*3^(2/3)*e*E^(5/2)*li^5*Po^5*sin(alpha)*Wy0*cos(alpha)^2+12*I*3^(2/3)*e*E^(7/2)*li^7*Po^3*sin(alpha)*Wy0+27*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^6*E*li^2*cos(alpha)^2+36*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*sin(alpha)*Wx0*E^(3/2)*li^3*cos(alpha)+18*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^6*sin(alpha)*Wx0*cos(alpha)*E^(1/2)*li+24*I*3^(2/3)*e*Po^9*cos(alpha)^2*E^(1/2)*li*sin(alpha)*Wy0-9*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^3*sin(alpha)*Wx0*E^2*li^4-9*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*sin(alpha)*Wx0*E*li^2+72*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^5*cos(alpha)^3*E*li^2-144*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^4*cos(alpha)^2*E^(3/2)*li^3-36*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^6*cos(alpha)^2*E^(1/2)*li-54*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^7*cos(alpha)^3*E^(1/2)*li-6*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E*li^2*Po^4-12*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^5*cos(alpha)^3*E^(1/2)*li+10*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*cos(alpha)^2*E*li^2-3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^2*li^4*Po*sin(alpha)*Wx0+36*E^(5/2)*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*li^4*Po^2+36*E^(3/2)*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*li^2*Po^4-6*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*Wz0*E^(1/2)*li+12*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E*li^2*Po^3*Wz0*cos(alpha)-6*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^6-3*3^(1/6)*e*E^(9/2)*li^9*Po*sin(alpha)*Wy0-4*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^7*cos(alpha)*E^(1/2)*li+2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*E^(1/2)*li*Po^6+6*3^(2/3)*e*E^(3/2)*li^3*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*Wz0+18*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^7*cos(alpha)^3*E^(1/2)*li-6*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*E^(3/2)*li^3*cos(alpha)-3*3^(1/6)*e*Po^9*sin(alpha)*Wy0*E^(1/2)*li-18*3^(1/6)*e*E^(5/2)*li^5*Po^5*sin(alpha)*Wy0-12*3^(1/6)*e*E^(7/2)*li^7*Po^3*sin(alpha)*Wy0-12*3^(1/6)*e*E^(3/2)*li^3*Po^7*sin(alpha)*Wy0+6*3^(2/3)*e*E^(5/2)*li^5*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*Wz0-36*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^6*cos(alpha)^4*E*li^2-21*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*cos(alpha)^2*E^2*li^4+52*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*cos(alpha)^3*E^(3/2)*li^3+24*3^(2/3)*e*E^(3/2)*li^3*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^5*cos(alpha)^5+42*3^(2/3)*e*E^(5/2)*li^5*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^3*cos(alpha)^3+2*3^(2/3)*e*E^(7/2)*li^7*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po*cos(alpha)-15*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^2*cos(alpha)^2*E^3*li^6-52*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^4*cos(alpha)^4*E^2*li^4-9*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^6*E*li^2*cos(alpha)^2+45*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^4*sin(alpha)*Wy0*cos(alpha)*E^2*li^4-8*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*cos(alpha)^2*Wz0*E^(1/2)*li-72*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^2*cos(alpha)^2*E^2*li^4-18*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^7*sin(alpha)*Wy0*E^(1/2)*li*cos(alpha)^2-72*E^3*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*li^5*Po*cos(alpha)-96*E^2*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*Po^3*cos(alpha)^3*li^3+60*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^4*sin(alpha)*Wy0*cos(alpha)^3*E^2*li^4+48*I*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^3*cos(alpha)^3*E^(3/2)*li^3+36*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*cos(alpha)^4*E*li^2-2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*Wz0*E^(1/2)*li+8*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^3*cos(alpha)^3*E*li^2*Wz0-2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E*li^2*Po^3*sin(alpha)*Wx0-16*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^2*cos(alpha)^2*E^(3/2)*li^3*Wz0+12*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*cos(alpha)^4*E*li^2+11*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^2*cos(alpha)^2*E^2*li^4+21*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^2*sin(alpha)*Wy0*cos(alpha)*E^3*li^6-2*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*E^(5/2)*li^5*Po*cos(alpha)-3*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po*sin(alpha)*Wy0*E^(7/2)*li^7+6*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^5*cos(alpha)*E^(1/2)*li+2*3^(2/3)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*E^4*li^8+3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Po^8+12*E^(1/2)*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*Po^6+12*E^(7/2)*lr*3^(1/6)*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)*li^6-3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^6-9*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^5*sin(alpha)*Wy0*E^(3/2)*li^3-48*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^2*cos(alpha)^2*E^(3/2)*li^3*Wz0+30*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^4*cos(alpha)^2*E*li^2+6*I*3^(1/6)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(5/6)*Po^5*Wz0*cos(alpha)+45*I*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/3)*Po^4*sin(alpha)*Wy0*cos(alpha)*E^2*li^4+12*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^6*cos(alpha)^2*E^(1/2)*li-18*3^(2/3)*e*((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(1/2)*Wz0*Po^5*cos(alpha)*E*li^2)*3^(5/6)/((E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3)^(2/3)/(E*li^2-2*Po*cos(alpha)*E^(1/2)*li+Po^2)^3];
        elseif((choice==1)||(choice==3))
            M=[[1/2*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*(3*L(1)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^2+2*L(1)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^2+2*L(1)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*B0(kg)^2-L(2)*A0(kg)*B0(kg))/(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(3/2),1/2*(-2*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*Wp*co*A0(kg)^2-2*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*Wp*co*C0(kg)^2-2*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*Wp*co*B0(kg)^2+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*B0(kg)*L(1)*A0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)+2*Wp*co*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^2+2*Wp*co*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^2+2*Wp*co*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*B0(kg)^2-2*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)*A0(kg)^2-2*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)*B0(kg)^2+2*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(2)*A0(kg)^2+2*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(2)*C0(kg)^2-2*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^3+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*B0(kg)^2*L(2))/(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(3/2),1/2*((Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*C0(kg)*L(1)*A0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)-2*epsil*B0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^2-2*epsil*B0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^2-(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(2)*C0(kg)*B0(kg)-2*epsil*B0(kg)^3*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))/(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(3/2)];[1/2*(-2*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(2)*C0(kg)^2-2*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*B0(kg)^2*L(2)+2*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^3-(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(2)*A0(kg)^2+2*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*Wp*co*A0(kg)^2+2*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*Wp*co*C0(kg)^2+2*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*Wp*co*B0(kg)^2+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*B0(kg)*L(1)*A0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)-2*Wp*co*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^2-2*Wp*co*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^2-2*Wp*co*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*B0(kg)^2+2*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)*A0(kg)^2+2*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)*B0(kg)^2)/(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(3/2),1/2*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*(3*L(1)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*B0(kg)^2+L(2)*A0(kg)*B0(kg)+2*L(1)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^2+2*L(1)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^2)/(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(3/2),1/2*(2*epsil*A0(kg)^3*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*C0(kg)*L(1)*B0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)+2*Wp*si*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^2+2*Wp*si*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^2+2*Wp*si*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*B0(kg)^2+2*epsil*A0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^2+2*epsil*A0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*B0(kg)^2+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(2)*C0(kg)*A0(kg))/(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(3/2)];[1/2*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*A0(kg)/(A0(kg)^2+C0(kg)^2+B0(kg)^2)*L(1)*(C0(kg)-1),1/2*(-2*epsil*Wp*si*A0(kg)^2-2*epsil*Wp*si*C0(kg)^2-2*epsil*Wp*si*B0(kg)^2+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*C0(kg)*L(1)*B0(kg)-2*Wp*si*A0(kg)^2-2*Wp*si*C0(kg)^2-2*Wp*si*B0(kg)^2-(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*B0(kg)*L(1))/(A0(kg)^2+C0(kg)^2+B0(kg)^2),1/2*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(1)*(2*A0(kg)^2+3*C0(kg)^2+2*B0(kg)^2-C0(kg))/(A0(kg)^2+C0(kg)^2+B0(kg)^2)]];
        elseif(choice==4)
            M=[[1/2*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*(2*L(1)*C0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*B0(kg)^2-L(2)*A0(kg)*B0(kg)*C0(kg)^2+5*L(3)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^2*C0(kg)^2+5*L(3)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^2*B0(kg)^2+L(3)*A0(kg)^2*C0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)-2*L(3)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)*B0(kg)^2+4*L(3)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^2*B0(kg)^2-L(1)*C0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^2+2*L(1)*C0(kg)^3*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)-L(2)*A0(kg)^3*B0(kg)-L(2)*A0(kg)*B0(kg)^3+3*L(3)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^4-2*L(3)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^3+2*L(3)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^4+2*L(3)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*B0(kg)^4)/(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(5/2),1/2*(4*Wp*co*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^2*C0(kg)^2+4*Wp*co*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^2*B0(kg)^2+4*Wp*co*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^2*B0(kg)^2-2*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*Wp*co*A0(kg)^4-2*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*Wp*co*C0(kg)^4-2*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*Wp*co*B0(kg)^4+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*B0(kg)*L(3)*A0(kg)^3*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)-4*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)*A0(kg)^2*B0(kg)^2+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*B0(kg)^3*L(3)*A0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)-3*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(1)*B0(kg)*A0(kg)*C0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)+3*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(3)*B0(kg)*C0(kg)*A0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)-4*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*Wp*co*A0(kg)^2*C0(kg)^2-4*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*Wp*co*A0(kg)^2*B0(kg)^2-4*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*Wp*co*C0(kg)^2*B0(kg)^2+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*B0(kg)*L(3)*A0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^2+2*Wp*co*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^4+2*Wp*co*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^4+2*Wp*co*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*B0(kg)^4+4*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(2)*A0(kg)^2*C0(kg)^2+3*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(2)*A0(kg)^2*B0(kg)^2+3*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(2)*C0(kg)^2*B0(kg)^2-2*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)*A0(kg)^4-4*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^2*C0(kg)^3-4*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^3*B0(kg)^2-2*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)*B0(kg)^4+2*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(2)*A0(kg)^4+2*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(2)*C0(kg)^4-2*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^5+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*B0(kg)^4*L(2))/(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(5/2),1/2*(-4*epsil*B0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^2*C0(kg)^2-(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*C0(kg)^2*L(1)*A0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*C0(kg)*L(3)*A0(kg)^3*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*C0(kg)^2*L(3)*A0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*C0(kg)^3*L(3)*A0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)-(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*C0(kg)*L(2)*B0(kg)*A0(kg)^2+2*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(1)*A0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*B0(kg)^2-2*(Ek*(A0(kg)^2+C0(kg)^2+...
                B0(kg)^2)^(1/2))^(1/2)*L(3)*A0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*B0(kg)^2-2*epsil*B0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^4-4*epsil*B0(kg)^3*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^2-2*epsil*B0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^4-4*epsil*B0(kg)^3*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^2-(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*C0(kg)^3*L(2)*B0(kg)-(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*C0(kg)*L(2)*B0(kg)^3+2*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(1)*A0(kg)^3*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)-2*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(3)*A0(kg)^3*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*C0(kg)*L(3)*A0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*B0(kg)^2-2*epsil*B0(kg)^5*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))/(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(5/2)];[1/2*(-4*Wp*co*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^2*C0(kg)^2-4*Wp*co*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^2*B0(kg)^2-4*Wp*co*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^2*B0(kg)^2+2*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*Wp*co*A0(kg)^4+2*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*Wp*co*C0(kg)^4+2*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*Wp*co*B0(kg)^4+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*B0(kg)*L(3)*A0(kg)^3*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)+4*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)*A0(kg)^2*B0(kg)^2+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*B0(kg)^3*L(3)*A0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)-3*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(1)*B0(kg)*A0(kg)*C0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)+3*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(3)*B0(kg)*C0(kg)*A0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)+4*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*Wp*co*A0(kg)^2*C0(kg)^2+4*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*Wp*co*A0(kg)^2*B0(kg)^2+4*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*Wp*co*C0(kg)^2*B0(kg)^2+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*B0(kg)*L(3)*A0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^2-2*Wp*co*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^4-2*Wp*co*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^4-2*Wp*co*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*B0(kg)^4-3*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(2)*A0(kg)^2*C0(kg)^2-3*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(2)*A0(kg)^2*B0(kg)^2-4*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(2)*C0(kg)^2*B0(kg)^2+2*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)*A0(kg)^4+4*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^2*C0(kg)^3+4*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^3*B0(kg)^2+2*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)*B0(kg)^4-(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(2)*A0(kg)^4-2*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(2)*C0(kg)^4+2*epsil*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^5-2*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*B0(kg)^4*L(2))/(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(5/2),1/2*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*(3*L(3)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*B0(kg)^4+L(2)*A0(kg)^3*B0(kg)+L(2)*A0(kg)*B0(kg)^3+2*L(3)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^4-2*L(3)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^3+2*L(3)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^4+2*L(1)*C0(kg)^3*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)-L(1)*C0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*B0(kg)^2+5*L(3)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^2*B0(kg)^2+L(3)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)*B0(kg)^2+5*L(3)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^2*B0(kg)^2+L(2)*A0(kg)*B0(kg)*C0(kg)^2+4*L(3)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^2*C0(kg)^2-...
                2*L(3)*A0(kg)^2*C0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)+2*L(1)*C0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^2)/(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(5/2),1/2*((Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*C0(kg)*L(3)*B0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^2+2*Wp*si*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^4+2*Wp*si*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^4+2*Wp*si*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*B0(kg)^4+4*epsil*A0(kg)^3*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^2+4*epsil*A0(kg)^3*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*B0(kg)^2+2*epsil*A0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^4+2*epsil*A0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*B0(kg)^4+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*C0(kg)*L(2)*A0(kg)^3+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*C0(kg)^3*L(2)*A0(kg)+2*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(1)*B0(kg)^3*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)-2*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(3)*B0(kg)^3*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)+2*epsil*A0(kg)^5*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)+4*Wp*si*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^2*C0(kg)^2+4*Wp*si*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^2*B0(kg)^2+4*Wp*si*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^2*B0(kg)^2+4*epsil*A0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*C0(kg)^2*B0(kg)^2-(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*C0(kg)^2*L(1)*B0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*C0(kg)^2*L(3)*B0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*C0(kg)^3*L(3)*B0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*C0(kg)*L(3)*B0(kg)^3*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*C0(kg)*L(2)*A0(kg)*B0(kg)^2+2*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(1)*B0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^2-2*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*L(3)*B0(kg)*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2)*A0(kg)^2)/(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(5/2)];[1/2*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*A0(kg)*(3*L(3)*C0(kg)^2-4*L(1)*C0(kg)^2-L(1)*A0(kg)^2-L(1)*B0(kg)^2+L(3)*A0(kg)^2*C0(kg)+L(3)*C0(kg)^3+L(3)*B0(kg)^2*C0(kg))/(A0(kg)^2+C0(kg)^2+B0(kg)^2)^2,1/2*(-4*epsil*Wp*si*A0(kg)^2*C0(kg)^2-4*epsil*Wp*si*A0(kg)^2*B0(kg)^2-4*epsil*Wp*si*C0(kg)^2*B0(kg)^2+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*B0(kg)*L(3)*A0(kg)^2*C0(kg)-2*epsil*Wp*si*A0(kg)^4-2*epsil*Wp*si*C0(kg)^4-2*epsil*Wp*si*B0(kg)^4-4*Wp*si*A0(kg)^2*C0(kg)^2-4*Wp*si*A0(kg)^2*B0(kg)^2-4*Wp*si*C0(kg)^2*B0(kg)^2-4*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*B0(kg)*C0(kg)^2*L(1)+3*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*B0(kg)*C0(kg)^2*L(3)-(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*B0(kg)*L(1)*A0(kg)^2+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*B0(kg)*L(3)*C0(kg)^3+(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*B0(kg)^3*L(3)*C0(kg)-2*Wp*si*A0(kg)^4-2*Wp*si*C0(kg)^4-2*Wp*si*B0(kg)^4-(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*B0(kg)^3*L(1))/(A0(kg)^2+C0(kg)^2+B0(kg)^2)^2,1/2*(Ek*(A0(kg)^2+C0(kg)^2+B0(kg)^2)^(1/2))^(1/2)*(3*L(1)*C0(kg)*A0(kg)^2+3*L(1)*C0(kg)*B0(kg)^2+5*L(3)*A0(kg)^2*C0(kg)^2+5*L(3)*C0(kg)^2*B0(kg)^2-4*L(3)*A0(kg)^2*C0(kg)-4*L(3)*B0(kg)^2*C0(kg)+4*L(3)*A0(kg)^2*B0(kg)^2-L(3)*C0(kg)^3+3*L(3)*C0(kg)^4+2*L(3)*A0(kg)^4+2*L(3)*B0(kg)^4)/(A0(kg)^2+C0(kg)^2+B0(kg)^2)^2]];
        end
        [V,Ei] = eig(M); lz=[lz; [Ei(1,1) Ei(2,2) Ei(3,3)]]; % V1: eigenvectors
        %         if(choice==0), Et=[Et; Et0]; end; %Reduced model eigenvalues at order e (ie O(e2)).
        stab(kg)=NaN; seuil=0;
        if((real(Ei(1,1))<=seuil)&&(real(Ei(2,2))<=seuil)&&((real(Ei(3,3))<=seuil))), stab(kg)=1; end;
    end
    varargout(1)={stab};  varargout(2)={lz};
    %     if(choice==0), varargout(3)={Et}; end;
end

%--------------------------------------------------------------------------
%---- Subfunction called in the main script -------------------------------
%--------------------------------------------------------------------------
function F = FullPrecframeSpheroid(a,b,c,Els,Bmag,W,e,Ek,Po,alpha,Lr,Li,Lsup,choice,rICB)
co=cos(alpha); si=sin(alpha); Wx=W(1); Wy=W(2); Wz=W(3);
Weq=[0 0 0]; % mantle : Weq=[0 0 1]; // Prec : Weq=[0 0 0];
Wpx=Wx+Weq(1); Wpy=Wy+Weq(2); Wpz=Wz+Weq(3); % mantle : Weq=[0 0 1]; // Prec : Weq=[0 0 0];
omega=sqrt(Wpx^2+Wpy^2+Wpz^2); % reduced model : omega=1;

if(~(imag(Ek)==0))
    beta=1; y=W;
    if(~(real(Ek)==0))
        zr=real(Ek); K=0.4; AA=3.3; BB=3; ep=sqrt((y(1)^2+y(2)^2+(1-y(3))^2));
        u0s = fzero(@(u0s) Sous_eq(u0s,zr,ep,imag(Ek),K,AA,BB),sqrt(ep*sqrt(imag(Ek))/cos(pi/4)));
        if(ep/sqrt(imag(Ek))>150)
            us2=u0s^2*cos(asin(BB*u0s/(K*ep))); %us2_yoder=0.002*ep^2;
            Cd=us2/ep^2; Lr=-Cd*45*pi/32; expo_rICB=5;
        else
            us2=u0s^2*cos(pi/4); %us2_yoder=us2;
            Cd=us2/ep^2; cal=2.62/5; Lr=-5*cal*sqrt(imag(Ek))./ep; expo_rICB=4;
        end
        if(~(abs(rICB)==0))
            if(abs(imag(rICB))==0)
                Lr=Lr.*(1+abs(rICB).^expo_rICB)./(1-abs(rICB).^5);
            elseif(abs(real(rICB))==0)
                Lr=Lr.*1./(1-abs(rICB).^5);
            else
                Lr=Lr.*abs(rICB).^expo_rICB./(1-abs(rICB).^5);
            end
        end
    end
    Ek=(y(1)^2+y(2)^2+(1-y(3))^2)/omega^beta;
end

if choice==4 %without NoSpinUp Condition
    %Lrx...Lix... are the viscous terms to add in the dW/dt equations
    Lrx=Lr*(Wpx*Wpz/omega^2); Lry=Lr*(Wpy*Wpz/omega^2);
    Lrz=Lr*((-omega^2+Wpz^2)/omega^2);
    Lix=Li*(Wpy/omega); Liy=Li*(-Wpx/omega); Liz=0;
    
    visc_nux=sqrt(Ek*omega)*(Lrx+Lix+Lsup*(Wpx*(1-Wpz/omega^2)));
    visc_nuy=sqrt(Ek*omega)*(Lry+Liy+Lsup*(Wpy*(1-Wpz/omega^2)));
    visc_nuz=sqrt(Ek*omega)*(Lrz+Liz+Lsup*(Wpz*(1-Wpz/omega^2)));
elseif(choice==3) %with NoSpinUp Condition y(6)=omega^2 in viscous terms
    Lrx=Lr*Wpx;             Lry=Lr*Wpy; Lrz=Lr*(Wpz-1);
    Lix=Li*(Wpy/omega);     Liy=Li*(-Wpx/omega); Liz=0;
    visc_nux=sqrt(Ek*omega)*(Lrx+Lix);
    visc_nuy=sqrt(Ek*omega)*(Lry+Liy);
    visc_nuz=sqrt(Ek*omega)*(Lrz+Liz);
    % visc_nux=sqrt(Ek)*(Lrx); visc_nuy=sqrt(Ek)*(Lry); visc_nuz=sqrt(Ek)*(Lrz);
end

Bx=Bmag(1); By=Bmag(2); Bz=Bmag(3);
Mx=(-(a^2*Bz^2*b^2+a^2*c^2*By^2+Bz^2*b^4+c^4*By^2)*a^2)*Wx+(By*a^2*Bx*c^2*(b^2+c^2))*Wy+(Bz*a^2*Bx*b^2*(b^2+c^2))*Wz;
My=(Bx*b^2*By*c^2*(a^2+c^2))*Wx+(-(a^4*Bz^2+a^2*Bz^2*b^2+c^4*Bx^2+b^2*c^2*Bx^2)*b^2)*Wy+(Bz*b^2*By*a^2*(a^2+c^2))*Wz;
Mz=(Bx*c^2*Bz*b^2*(a^2+b^2))*Wx+(By*c^2*Bz*a^2*(a^2+b^2))*Wy+(-c^2*(a^4*By^2+a^2*c^2*By^2+b^4*Bx^2+b^2*c^2*Bx^2))*Wz;
Tmag=Els*[Mx My Mz]/((a^2+b^2)*(b^2+c^2)*(a^2+c^2));

F = [Po*co*Wy-e*(Po*co*Wy+Wy*Wz)+visc_nux+Tmag(1);
    Po*si*Wz-Po*co*Wx+e*(Po*co*Wx+Wx*Wz)+visc_nuy+Tmag(2);
    -Po*si*Wy-e*Po*si*Wy+visc_nuz+Tmag(3)];


function F = GeneralCebron(Els,Bmag,W,a,b,c,Ek,Wpx,Wpy,Wpz,L,rICB)
Wx=W(1); Wy=W(2); Wz=W(3); Lr=L(1); Li=L(2); Lsup=L(3);
omega=sqrt(Wx^2+Wy^2+Wz^2); % reduced model : omega=1;

if(~(imag(Ek)==0))
    beta=1; y=W;
    if(~(real(Ek)==0))
        zr=real(Ek); K=0.4; AA=3.3; BB=3; ep=sqrt((y(1)^2+y(2)^2+(1-y(3))^2));
        u0s = fzero(@(u0s) Sous_eq(u0s,zr,ep,imag(Ek),K,AA,BB),sqrt(ep*sqrt(imag(Ek))/cos(pi/4)));
        if(ep/sqrt(imag(Ek))>150)
            us2=u0s^2*cos(asin(BB*u0s/(K*ep))); %us2_yoder=0.002*ep^2;
            Cd=us2/ep^2; Lr=-Cd*45*pi/32; expo_rICB=5;
        else
            us2=u0s^2*cos(pi/4); %us2_yoder=us2;
            Cd=us2/ep^2; cal=2.62/5; Lr=-5*cal*sqrt(imag(Ek))./ep; expo_rICB=4;
        end
        if(~(abs(rICB)==0))
            if(abs(imag(rICB))==0)
                Lr=Lr.*(1+abs(rICB).^expo_rICB)./(1-abs(rICB).^5);
            elseif(abs(real(rICB))==0)
                Lr=Lr.*1./(1-abs(rICB).^5);
            else
                Lr=Lr.*abs(rICB).^expo_rICB./(1-abs(rICB).^5);
            end
        end
    end
    Ek=(y(1)^2+y(2)^2+(1-y(3))^2)/omega^beta;
end


Lrx=Lr*(Wx*Wz/omega^2); Lry=Lr*(Wy*Wz/omega^2);
Lrz=Lr*((-omega^2+Wz^2)/omega^2);
Lix=Li*(Wy/omega); Liy=Li*(-Wx/omega); Liz=0;
visc_nux=sqrt(Ek*omega)*(Lrx+Lix+Lsup*(Wx*(1-Wz/omega^2)));
visc_nuy=sqrt(Ek*omega)*(Lry+Liy+Lsup*(Wy*(1-Wz/omega^2)));
visc_nuz=sqrt(Ek*omega)*(Lrz+Liz+Lsup*(Wz*(1-Wz/omega^2)));

% Lrx=Lr*Wx;             Lry=Lr*Wy; Lrz=Lr*(Wz-1);
% Lix=Li*(Wy/omega);     Liy=Li*(-Wx/omega); Liz=0;
% visc_nux=sqrt(Ek*omega)*(Lrx+Lix);
% visc_nuy=sqrt(Ek*omega)*(Lry+Liy);
% visc_nuz=sqrt(Ek*omega)*(Lrz+Liz);

Bx=Bmag(1); By=Bmag(2); Bz=Bmag(3);
Mx=(-(a^2*Bz^2*b^2+a^2*c^2*By^2+Bz^2*b^4+c^4*By^2)*a^2)*Wx+(By*a^2*Bx*c^2*(b^2+c^2))*Wy+(Bz*a^2*Bx*b^2*(b^2+c^2))*Wz;
My=(Bx*b^2*By*c^2*(a^2+c^2))*Wx+(-(a^4*Bz^2+a^2*Bz^2*b^2+c^4*Bx^2+b^2*c^2*Bx^2)*b^2)*Wy+(Bz*b^2*By*a^2*(a^2+c^2))*Wz;
Mz=(Bx*c^2*Bz*b^2*(a^2+b^2))*Wx+(By*c^2*Bz*a^2*(a^2+b^2))*Wy+(-c^2*(a^4*By^2+a^2*c^2*By^2+b^4*Bx^2+b^2*c^2*Bx^2))*Wz;
Tmag=Els*[Mx My Mz]/((a^2+b^2)*(b^2+c^2)*(a^2+c^2));

F = [2*a^2*((1/(a^2+c^2)-1/(a^2+b^2))*Wy*Wz-(Wpy*Wz/(a^2+b^2)-Wpz*Wy/(a^2+c^2)))+visc_nux+Tmag(1);
    2*b^2*((1/(b^2+a^2)-1/(b^2+c^2))*Wz*Wx-(Wpz*Wx/(b^2+c^2)-Wpx*Wz/(b^2+a^2)))+visc_nuy+Tmag(2);
    2*c^2*((1/(c^2+b^2)-1/(c^2+a^2))*Wx*Wy-(Wpx*Wy/(c^2+a^2)-Wpy*Wx/(c^2+b^2)))+visc_nuz]+Tmag(3);

function dy = triaxPrecFrame_eq(t,y,a,b,c,Ek,Wpx,Wpy,Wpz,Po,alpha,L,Els,Bmag,choice,rICB)
Lr=L(1); Li=L(2); Lsup=L(3); omega=sqrt(y(1)^2+y(2)^2+y(3)^2);

if(~(imag(Ek)==0))
    if((choice==8)||(choice==10))
        beta=1;
    elseif((choice==9)||(choice==12))
        beta=0;
    elseif((choice==7)||(choice==11))
        beta=1;
    end
    if(~(real(Ek)==0))
        zr=real(Ek); K=0.4; AA=3.3; BB=3; ep=sqrt((y(1)^2+y(2)^2+(1-y(3))^2));
        u0s = fzero(@(u0s) Sous_eq(u0s,zr,ep,imag(Ek),K,AA,BB),sqrt(ep*sqrt(imag(Ek))/cos(pi/4)));
        if(ep/sqrt(imag(Ek))>150)
            us2=u0s^2*cos(asin(BB*u0s/(K*ep))); %us2_yoder=0.002*ep^2;
            Cd=us2/ep^2; Lr=-Cd*45*pi/32; expo_rICB=5;
        else
            us2=u0s^2*cos(pi/4); %us2_yoder=us2;
            Cd=us2/ep^2; cal=2.62/5; Lr=-5*cal*sqrt(imag(Ek))./ep; expo_rICB=4;
        end
        if(~(abs(rICB)==0))
            if(abs(imag(rICB))==0)
                Lr=Lr.*(1+abs(rICB).^expo_rICB)./(1-abs(rICB).^5);
            elseif(abs(real(rICB))==0)
                Lr=Lr.*1./(1-abs(rICB).^5);
            else
                Lr=Lr.*abs(rICB).^expo_rICB./(1-abs(rICB).^5);
            end
        end
    end
    Ek=(y(1)^2+y(2)^2+(1-y(3))^2)/omega^beta;
end

if((choice==7)||(choice==11)) %without NoSpinUp Condition
    %Lrx...Lix... are the terms to add in the dW/dt equations, they already
    %account for the L matrix
    Lrx=Lr*(y(1)*y(3)/omega^2); Lry=Lr*(y(2)*y(3)/omega^2);
    Lrz=Lr*((-omega^2+y(3)^2)/omega^2);
    Lix=Li*(y(2)/omega); Liy=Li*(-y(1)/omega); Liz=0;
    
    visc_nux=sqrt(Ek*omega)*(Lrx+Lix+Lsup*(y(1)*(1-y(3)/omega^2)));
    visc_nuy=sqrt(Ek*omega)*(Lry+Liy+Lsup*(y(2)*(1-y(3)/omega^2)));
    visc_nuz=sqrt(Ek*omega)*(Lrz+Liz+Lsup*(y(3)*(1-y(3)/omega^2)));
elseif((choice==8)||(choice==9)||(choice==10)||(choice==12)) %with NoSpinUp Condition y(3)=omega^2
    Lrx=Lr*y(1); Lry=Lr*y(2); Lrz=Lr*(y(3)-1);
    Lix=Li*(y(2)/omega); Liy=Li*(-y(1)/omega); Liz=0;
    if((choice==8)||(choice==10))  % Generalized model with NoSpinUp Condition y(3)=omega^2
        visc_nux=sqrt(Ek*omega)*(Lrx+Lix);
        visc_nuy=sqrt(Ek*omega)*(Lry+Liy);
        visc_nuz=sqrt(Ek*omega)*(Lrz+Liz);
    elseif((choice==9)||(choice==12)) % Reduced model
        visc_nux=sqrt(Ek)*(Lrx+Li*y(2));
        visc_nuy=sqrt(Ek)*(Lry-Li*y(1));
        visc_nuz=sqrt(Ek)*(Lrz);
    end
end
co=cos(alpha); si=sin(alpha);

Bx=Bmag(1); By=Bmag(2); Bz=Bmag(3);
Mx=(-(a^2*Bz^2*b^2+a^2*c^2*By^2+Bz^2*b^4+c^4*By^2)*a^2)*y(1)+(By*a^2*Bx*c^2*(b^2+c^2))*y(2)+(Bz*a^2*Bx*b^2*(b^2+c^2))*y(3);
My=(Bx*b^2*By*c^2*(a^2+c^2))*y(1)+(-(a^4*Bz^2+a^2*Bz^2*b^2+c^4*Bx^2+b^2*c^2*Bx^2)*b^2)*y(2)+(Bz*b^2*By*a^2*(a^2+c^2))*y(3);
Mz=(Bx*c^2*Bz*b^2*(a^2+b^2))*y(1)+(By*c^2*Bz*a^2*(a^2+b^2))*y(2)+(-c^2*(a^4*By^2+a^2*c^2*By^2+b^4*Bx^2+b^2*c^2*Bx^2))*y(3);
Tmag=Els*[Mx My Mz]/((a^2+b^2)*(b^2+c^2)*(a^2+c^2));

if((choice==7)||(choice==8)||(choice==9))
    %Triaxe
    dy = zeros(3,1);    % a column vector
    dy(1)=1/(c^2+b^2)/(a^2+c^2)/(b^2+a^2)*(-a^4*y(2)*c^2-a^4*y(2)*b^2+y(2)*c^4*b^2+b^4*a^2*y(2)+b^4*c^2*y(2)+2*cos(t)*Po*si*sin(t)*a^4*y(3)*c^2+2*cos(t)*Po*si*sin(t)*a^4*y(3)*b^2+2*cos(t)*Po*si*sin(t)*c^4*a^2*y(3)-2*cos(t)*Po*co*a^4*y(1)*sin(t)*c^2-2*sin(t)*Po*si*cos(t)*c^4*b^2*y(3)+2*sin(t)*b^4*Po*co*y(1)*cos(t)*c^2-2*sin(t)*Po*si*cos(t)*b^4*y(3)*a^2-2*sin(t)*Po*si*cos(t)*b^4*y(3)*c^2+2*Po*co*b^4*a^2*y(2)+2*Po*co*a^4*y(2)*b^2+2*b^4*Po*co*y(2)*c^2+2*Po*co*b^2*a^2*y(2)*c^2+2*b^2*a^4*y(2)*y(3)-2*b^2*c^4*y(2)*y(3)-2*b^2*a^4*y(2)*y(3)*cos(t)^2+2*b^2*c^4*y(2)*y(3)*cos(t)^2-2*b^4*Po*co*y(2)*c^2*cos(t)^2+2*a^4*y(2)*cos(t)^2*c^2+2*a^4*y(2)*cos(t)^2*b^2+2*c^4*a^2*y(2)*cos(t)^2-2*y(2)*cos(t)^2*c^4*b^2-2*b^4*a^2*y(2)*cos(t)^2-2*y(2)*cos(t)^2*b^4*c^2+2*cos(t)*Po*si*sin(t)*b^4*a^2+2*cos(t)*Po*si*sin(t)*b^4*c^2-2*cos(t)*Po*si*sin(t)*a^4*b^2+2*Po*co*a^4*y(2)*cos(t)^2*c^2-2*cos(t)*b^4*a^2*y(1)*sin(t)*y(3)+2*cos(t)*c^4*a^2*y(1)*sin(t)*y(3)-2*cos(t)*Po*si*sin(t)*c^2*a^4+2*cos(t)*Po*si*sin(t)*c^4*b^2-2*cos(t)*Po*si*sin(t)*c^4*a^2+2*sin(t)*b^2*a^4*y(1)*cos(t)*y(3)-2*sin(t)*b^2*c^4*y(1)*cos(t)*y(3)-2*cos(t)*a^4*y(1)*sin(t)*c^2-2*cos(t)*a^4*y(1)*sin(t)*b^2-2*cos(t)*c^4*a^2*y(1)*sin(t)+2*b^4*a^2*y(2)*cos(t)^2*y(3)-2*c^4*a^2*y(2)*cos(t)^2*y(3)+2*sin(t)*y(1)*cos(t)*c^4*b^2+2*sin(t)*b^4*a^2*y(1)*cos(t)+2*sin(t)*b^4*c^2*y(1)*cos(t)-c^4*a^2*y(2))+visc_nux+Tmag(1);
    dy(2)=-(a^4*y(1)*c^2+a^4*y(1)*b^2-2*b^4*a^2*y(1)*y(3)*cos(t)^2+2*c^4*a^2*y(1)*y(3)*cos(t)^2+2*Po*si*a^4*y(3)*c^2*cos(t)^2+2*Po*si*a^4*y(3)*b^2*cos(t)^2+2*Po*si*c^4*a^2*y(3)*cos(t)^2-2*Po*co*a^4*y(1)*c^2*cos(t)^2-y(1)*c^4*b^2-2*sin(t)*Po*co*a^4*y(2)*cos(t)*c^2+2*cos(t)*b^4*Po*co*y(2)*sin(t)*c^2-2*sin(t)*b^4*a^2*y(2)*cos(t)*y(3)+2*sin(t)*c^4*a^2*y(2)*cos(t)*y(3)-2*Po*si*cos(t)^2*c^4*b^2*y(3)+2*b^4*Po*co*y(1)*cos(t)^2*c^2-2*Po*si*cos(t)^2*b^4*y(3)*a^2-2*Po*si*cos(t)^2*b^4*y(3)*c^2+2*cos(t)*b^2*a^4*y(2)*sin(t)*y(3)-2*cos(t)*b^2*c^4*y(2)*sin(t)*y(3)+2*Po*co*b^2*a^2*y(1)*c^2-2*Po*si*c^2*a^2*y(3)*b^2+2*b^4*a^2*y(1)*y(3)-2*sin(t)*a^4*y(2)*cos(t)*c^2-2*sin(t)*a^4*y(2)*cos(t)*b^2-2*sin(t)*c^4*a^2*y(2)*cos(t)+2*cos(t)*y(2)*sin(t)*c^4*b^2+2*cos(t)*b^4*a^2*y(2)*sin(t)+2*cos(t)*b^4*c^2*y(2)*sin(t)-2*Po*si*cos(t)^2*a^4*c^2-2*Po*si*cos(t)^2*a^4*b^2+2*b^2*a^4*y(1)*cos(t)^2*y(3)-2*b^2*c^4*y(1)*cos(t)^2*y(3)+2*Po*si*cos(t)^2*c^4*b^2-2*Po*si*cos(t)^2*c^4*a^2+2*Po*si*cos(t)^2*c^2*b^4+2*Po*si*cos(t)^2*b^4*a^2-2*Po*si*a^4*y(3)*c^2-2*Po*si*a^4*y(3)*b^2-2*Po*si*c^4*a^2*y(3)+2*Po*co*a^4*y(1)*c^2+2*Po*co*a^4*y(1)*b^2-2*y(1)*cos(t)^2*c^2*a^4+2*y(1)*cos(t)^2*c^4*b^2-2*y(1)*cos(t)^2*c^4*a^2+2*b^4*a^2*y(1)*cos(t)^2-2*b^2*a^4*y(1)*cos(t)^2+2*b^4*c^2*y(1)*cos(t)^2-2*c^4*a^2*y(1)*y(3)+Po*si*a^4*c^2-Po*si*c^4*b^2+2*Po*co*b^4*a^2*y(1)+Po*si*a^4*b^2-Po*si*c^2*b^4-Po*si*b^4*a^2+Po*si*c^4*a^2-b^4*c^2*y(1)-b^4*a^2*y(1)+c^4*a^2*y(1))/(c^2+b^2)/(a^2+c^2)/(b^2+a^2)+visc_nuy+Tmag(2);
    dy(3)=2*c^2*(-Po*si*sin(t)*a^2*y(1)*cos(t)-Po*si*a^2*y(2)+Po*si*a^2*y(2)*cos(t)^2-a^2*y(1)^2*cos(t)*sin(t)+2*a^2*y(1)*cos(t)^2*y(2)-a^2*y(1)*y(2)+a^2*y(2)^2*sin(t)*cos(t)+Po*si*cos(t)*b^2*y(1)*sin(t)-Po*si*cos(t)^2*b^2*y(2)-Po*si*c^2*y(2)+b^2*y(1)^2*cos(t)*sin(t)-2*b^2*y(1)*cos(t)^2*y(2)+b^2*y(1)*y(2)-b^2*y(2)^2*sin(t)*cos(t))/(c^2+b^2)/(a^2+c^2)+visc_nuz+Tmag(3);
elseif((choice==10)||(choice==11)||(choice==12))
    dy = zeros(3,1);    % a column vector
    dy(1)=2*a^2*((1/(a^2+c^2)-1/(a^2+b^2))*y(2)*y(3)-(Wpy*y(3)/(a^2+b^2)-Wpz*y(2)/(a^2+c^2)))+visc_nux+Tmag(1);
    dy(2)=2*b^2*((1/(b^2+a^2)-1/(b^2+c^2))*y(3)*y(1)-(Wpz*y(1)/(b^2+c^2)-Wpx*y(3)/(b^2+a^2)))+visc_nuy+Tmag(2);
    dy(3)=2*c^2*((1/(c^2+b^2)-1/(c^2+a^2))*y(1)*y(2)-(Wpx*y(2)/(c^2+a^2)-Wpy*y(1)/(c^2+b^2)))+visc_nuz+Tmag(3);
end

function [a10,a20,eps,eps2,tanTheta,xM,yM,zM,X4,Y4,Z4]=ShearElli_eq(a,b,c,A,B,C,D)

K=sqrt(A.^2+B.^2+C.^2);

b1t=1; c1t=1;
a1 = -(b1t.*B+c1t.*C)./(A.*sqrt((b1t.*B+c1t.*C).^2./A.^2+b1t.^2+c1t.^2));
b1 = b1t./sqrt((b1t.*B+c1t.*C).^2./A.^2+b1t.^2+c1t.^2);
c1 = c1t./sqrt((b1t.*B+c1t.*C).^2./A.^2+b1t.^2+c1t.^2);

a2=A.^2.*(c1.*B-b1.*C)./(K.*(b1.^2.*A.^2+b1.^2.*B.^2+2.*b1.*c1.*C.*B+c1.^2.*A.^2+c1.^2.*C.^2));
b2=-A.*(c1.*A.^2+b1.*B.*C+c1.*C.^2)./(K.*(b1.^2.*A.^2+b1.^2.*B.^2+2.*b1.*c1.*C.*B+c1.^2.*A.^2+c1.^2.*C.^2));
c2=A.*(b1.*A.^2+b1.*B.^2+c1.*C.*B)./((b1.^2.*A.^2+b1.^2.*B.^2+2.*b1.*c1.*C.*B+c1.^2.*A.^2+c1.^2.*C.^2).*K);

alpha=(a1.^2.*b.^2.*c.^2+b1.^2.*a.^2.*c.^2+c1.^2.*a.^2.*b.^2)./(a.^2.*b.^2.*c.^2);
beta=(a2.^2.*b.^2.*c.^2+b2.^2.*a.^2.*c.^2+c2.^2.*a.^2.*b.^2)./(a.^2.*b.^2.*c.^2);
gam=(a1.*a2.*b.^2.*c.^2+b1.*b2.*a.^2.*c.^2+c1.*c2.*a.^2.*b.^2)./(a.^2.*b.^2.*c.^2);
xi1=-D.*(a1.*A.*b.^2.*c.^2+b1.*B.*a.^2.*c.^2+c1.*C.*a.^2.*b.^2)./(K.^2.*a.^2.*b.^2.*c.^2);
xi2=-D.*(a2.*A.*b.^2.*c.^2+b2.*B.*a.^2.*c.^2+c2.*C.*a.^2.*b.^2)./(K.^2.*a.^2.*b.^2.*c.^2);
cte=(A.^2.*D.^2.*b.^2.*c.^2+B.^2.*D.^2.*a.^2.*c.^2+C.^2.*D.^2.*a.^2.*b.^2-a.^2.*K.^4.*b.^2.*c.^2)./a.^2./K.^4./b.^2./c.^2;

alpha=alpha./abs(cte); beta=beta./abs(cte); gam=gam./abs(cte); xi1=xi1./cte; xi2=xi2./abs(cte);

TrM=alpha+beta; detM=alpha.*beta-gam.^2;
lambda1=TrM./2+1./2.*sqrt(abs(TrM.^2-4.*detM)); a10=1./sqrt(lambda1);
lambda2=TrM./2-1./2.*sqrt(abs(TrM.^2-4.*detM)); a20=1./sqrt(lambda2);
eps=abs(a20.^2-a10.^2)./(a20.^2+a10.^2);

xM=(gam.*xi2-xi1.*beta)./detM;
yM=(gam.*xi1-xi2.*alpha)./detM;
zM=-A./C.*xM-B./C.*yM-D./C;

X4=D.*(A.^2.*a.^2-a.^2.*C.^2+2.*c.^2.*C.^2-B.^2.*a.^2+2.*b.^2.*B.^2).*A./((A.^2.*a.^2+c.^2.*C.^2+b.^2.*B.^2).*(A.^2+B.^2+C.^2)); % centre ds rep. de l'ellipsoide
Y4=B.*D.*(2.*A.^2.*a.^2-A.^2.*b.^2+2.*c.^2.*C.^2-C.^2.*b.^2+b.^2.*B.^2)./((A.^2.*a.^2+c.^2.*C.^2+b.^2.*B.^2).*(A.^2+B.^2+C.^2)); % centre ds rep. de l'ellipsoide
Z4=C.*D.*(2.*A.^2.*a.^2-A.^2.*c.^2+2.*b.^2.*B.^2+c.^2.*C.^2-B.^2.*c.^2)./((A.^2.*a.^2+c.^2.*C.^2+b.^2.*B.^2).*(A.^2+B.^2+C.^2)); % centre ds rep. de l'ellipsoide

tanTheta=sqrt((B.^2.*C.^2.*(c.^2-b.^2).^2+A.^2.*C.^2.*(a.^2-c.^2).^2+A.^2.*B.^2.*(a.^2-b.^2).^2))./abs(a.^2.*A.^2+c.^2.*C.^2+b.^2.*B.^2);

Gam=TrM.^2./detM; Gamt=2-Gam;
Z=-Gamt./2+1./2.*sqrt(Gamt.^2-4);
eps2=abs(1-Z)./(1+Z);

% a1=0; a2=0;xM=0; yM=0; eps=0; eps2=0;

% -- Spinover damping coeff for ellipsoids --------------------------------
function L=LSO(a,b,c,choice,varargin)

if((choice==4)||(choice==6)||(choice==7)||(choice==11))
    L(3)=-(1./2).*pi.^(3./2).*sqrt(2).*hypergeom([-1./4, 1./2],[3./4],1-c.^2)./(c.*gamma(3./4).^2); % Lsup
else
    L(3)=0;
end

if(a==b)
    e2=1-(1-(1-c/a)).^2; % Zhang eccentricity
    F = @(t) 10./3.*1./(2-e2).^3.*(1-e2).^(1./2).*((1-e2.*(1-t.^2)).^(1./4).*(1./(2-e2).*(1-e2.*(1-t.^2)).^(1./2)+t).*(3./4.*t.*(2-e2)./(1-e2).^(1./2)-3./4.*(2-e2).*(1-e2+e2.*t.^2).^(1./2)./(1-e2).^(1./2))./abs(1./(2-e2).*(1-e2.*(1-t.^2)).^(1./2)+t).^(3./2).*((1-t.^2).^(1./2).*(3./2.*(1-e2).^(1./2).*(1-t.^2).^(1./2)-3./2.*t.^2.*(1-e2).^(1./2)./(1-t.^2).^(1./2))+3./2.*(1-e2.*(1-t.^2)).^(1./2).*t.*(1-e2).^(1./2)));
    G = @(t) 10./3.*1./(2-e2).^3.*(1-e2).^(1./2).*((1-e2.*(1-t.^2)).^(1./4).*(3./4.*t.*(2-e2)./(1-e2).^(1./2)-3./4.*(2-e2).*(1-e2+e2.*t.^2).^(1./2)./(1-e2).^(1./2))./abs(1./(2-e2).*(1-e2.*(1-t.^2)).^(1./2)+t).^(1./2).*((1-t.^2).^(1./2).*(3./2.*(1-e2).^(1./2).*(1-t.^2).^(1./2)-3./2.*t.^2.*(1-e2).^(1./2)./(1-t.^2).^(1./2))+3./2.*(1-e2.*(1-t.^2)).^(1./2).*t.*(1-e2).^(1./2)));
    L(1) = quadl(F,-1,1,1e-13); L(2) = quadl(G,-1,1,1e-13); % L=[lr li lsup] (Noir & Cebron 2013)
    
elseif(~(a==b))
    % Numerical integration points in phi and theta direction
    np=300; nt=300;
    phivec = 0:2*pi/np:2*pi;
    thetavec = 0:pi/nt:pi;
    
    % beta and cb are the equatorial ellipticity and non-dimensional aspect
    % ratio (see (3.23))
    %R=1; beta = (a^2-b^2)/(a^2+b^2); cb = c/sqrt((a^2+b^2)/2);
    %a = R*sqrt(1+beta);
    %b = R*sqrt(1-beta);
    %c = R*cb;
    a2 = a^2; b2 = b^2; c2 = c^2;
    
    % sigma=eigenfrequency of spin-over mode
    sigma = 2*a*b/sqrt(a2+c2)/sqrt(b2+c2);
    s = b/a*sqrt((a2+c2)/(b2+c2));
    
    % Denominator of (3.36)
    e=4*pi/15*a*b*c*(1/a2+1/c2 + s^2*(1/b2+1/c2));
    
    for l=1:nt+1
        for m=1:np+1
            phi = phivec(m);
            theta = thetavec(l);
            
            % Cartesian coordinates on the ellipsoidal surface
            x = a*sin(theta)*cos(phi);
            y = b*sin(theta)*sin(phi);
            z = c*cos(theta);
            
            % Normal vector on ellipsoidal surface
            n = [x/a2 y/b2 z/c2];
            nnorm = sqrt(x^2/a^4+y^2/b^4+z^2/c^4);
            n = n/nnorm;
            
            % Rotation axis
            k = [0 0 1];
            
            nk = dot(n,k);
            bp = sigma + 2*nk;
            bm = sigma - 2*nk;
            
            % Spin-over mode
            q = [z/c2 0 -x/a2] - complex(0,1)*s*[0 -z/c2 y/b2];
            
            alpha1 = dot(n,cross(k,q)) - complex(0,1)*dot(k,q);
            alpha2 = dot(n,cross(k,q)) + complex(0,1)*dot(k,q);
            
            beta1 = alpha1*alpha1'*sqrt(abs(bp))*(1+complex(0,1)*sign(bp));
            beta2 = alpha2*alpha2'*sqrt(abs(bm))*(1+complex(0,1)*sign(bm));
            
            % Surface element
            ds = a*b*c*sin(theta)*nnorm;
            f(l,m) = 1/2^(3/2)/(1-nk^2+1.e-12)*(beta1+beta2)*ds;
        end
        g(l) = trapz(phivec(:),f(l,:));
    end
    lambda = trapz(thetavec,g(:)); % Numerator of (3.36)
    lambda=-lambda/e; L(1)=-real(lambda); L(2)=-imag(lambda);
end
if(length(varargin)>=1)
    Ek=varargin{1};
    L(1)= L(1)-1.36*Ek^(0.27);
    L(2)= L(2)+1.25*Ek^(0.20777);
end


%%
function eq=Sous_eq(u0s,zr,U,Ek,K,A,B)
if(U/sqrt(Ek)>150)
    if(abs(zr*u0s/Ek)>60)
        %display('Turbulent BL (Sous+13)');
        z0=zr/30;
    else
        z0=0.11*Ek/u0s;
    end
    Rof0=abs(u0s/(2*z0));
    eq=u0s^2/U^2-K^2/((log(Rof0)-A)^2+B^2);
else
    eq=u0s^2*cos(pi/4)-U*sqrt(Ek);
end



