%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                  _.-,
%              .--'  '-._
%           _/`-  _      '.
%          '----'._`.----. \
%                   `     \;
%                         ;_\
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Goal: validation test cases of the script FLIPPER
% Author: D. Cebron
% Affiliation: ISTerre, Grenoble (France)
% Date: Frebruary, 20, 2014
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Validation on a single-solution axi case
clear all; clc; close all;
a=1; b=1; c=0.9; Ek=1e-3;
Ro=0.1; Omega_p=0.3; 

Wpx=Ro*cos(pi/4)*sign(Omega_p); Wpy=Ro*sin(pi/4)*sign(Omega_p);
Wpz=Omega_p*cos(asin(Ro./abs(Omega_p)));

[AA,BB,CC]=Flipper(a,b,c,1,Wpx,Wpy,Wpz,-1);
[A0,B0,C0]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,0);
[A1,B1,C1]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,1);
[A2,B2,C2]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,2);
[A3,B3,C3]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,3);
[A4,B4,C4]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,4);
[A5,B5,C5]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,5);
[A6,B6,C6]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,6);
[A7,B7,C7]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,7);
[A8,B8,C8]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,8);
[A9,B9,C9]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,9);
[A10,B10,C10]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,10);
[A11,B11,C11]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,11);
[A12,B12,C12]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,12);

Compa=[AA,BB,CC; A0,B0,C0; A1 B1 C1; A2 B2 C2;  A3 B3 C3;  A4 B4 C4; A5 B5 C5; A6 B6 C6; A7(end) B7(end) C7(end); ...
    A8(end) B8(end) C8(end); A9(end) B9(end) C9(end); A10(end) B10(end) C10(end); A11(end) B11(end) C11(end); A12(end) B12(end) C12(end)]

Ek=i*Ek;
[jA3,jB3,jC3]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,3);
[jA4,jB4,jC4,jL4]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,4);
[jA7,jB7,jC7,jL7]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,7);
[jA8,jB8,jC8]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,8);
[jA9,jB9,jC9]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,9);
[jA10,jB10,jC10,jL10]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,10);
[jA11,jB11,jC11,jL11]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,11);
[jA12,jB12,jC12]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,12);

Compa2=[jA3 jB3 jC3; jA4 jB4 jC4; jA7(end) jB7(end) jC7(end); ...
    jA8(end) jB8(end) jC8(end); jA9(end) jB9(end) jC9(end); jA10(end) jB10(end) jC10(end); jA11(end) jB11(end) jC11(end); jA12(end) jB12(end) jC12(end)]


%% Validation on a multiple-solution axi case
clear all; clc; close all;
a=1; b=1; c=1/0.9; Ek=1e-6; L=[-2.62 0.25 -0.5];
Wpx=0.01; Wpy=0; Wpz=0; T=0:10:1e4;

[A0,B0,C0]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,0,0,1,[1 1 1],0,0,T);
[A1,B1,C1]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,1,0,1,[1 1 1],0,0,T);
[A2,B2,C2]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,2,0,1,[1 1 1],0,0,T);
[A3,B3,C3]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,3,0,1,[1 1 1],0,0,T);
[A4,B4,C4]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,4,0,1,[1 1 1],0,0,T);
[A5,B5,C5]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,5,0,1,[1 1 1],0,0,T);
[A6,B6,C6]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,6,0,1,[1 1 1],0,0,T);
[A7,B7,C7]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,7,0,1,[1 1 1],0,0,T);
[A8,B8,C8]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,8,0,1,[1 1 1],0,0,T);
[A9,B9,C9]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,9,0,1,[1 1 1],0,0,T);
[A10,B10,C10]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,10,0,1,[1 1 1],0,0,T);
[A11,B11,C11]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,11,0,1,[1 1 1],0,0,T);
[A12,B12,C12]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,12,0,1,[1 1 1],0,0,T);

Compa=[C1; C2; C3; C4; C5; C6]
Compa2=[C0 C7(end) C8(end) C9(end) C10(end) C11(end) C12(end)]

%% Validation of angles vs. Kerswell (1993) : Poincare flow
clear all; clc; close all;
a=1; b=1; c=1.6; Ek=0;
Ro=0.1; Omega_p=0.3; 

Wpx=Ro*cos(0)*sign(Omega_p); Wpy=Ro*sin(0)*sign(Omega_p);
Wpz=Omega_p*cos(asin(Ro./abs(Omega_p)));

[AA,BB,CC,L,compteur,eq1,eq2,eq3,eps,beta,theta2D,phi2D,NormCenter]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,-1);

eta=(a/c)^2-1; mu=2*Wpx/(eta+2*(1+eta)*Wpz);
Compa=[AA,BB,CC,eps,beta,theta2D,phi2D,NormCenter; ...
    (2+eta)/(eta+2*(1+eta)*Wpz)*Wpx 0 1 eta*mu/(2+(2+eta)*mu^2) eta*mu^2/(2+(2+eta)*mu^2) ...
    atan(2*Wpx/(eta+2*(1+eta)*Wpz)) 0 atan(2*eta*(eta+2*(1+eta)*Wpz)*Wpx/((eta+2*(1+eta)*Wpz)^2+4*(1+eta)*Wpx^2))]

figure(1); 
plot([0 0],[0 1],'-k','Linewidth',2); hold on; 
xx=0:0.01:1; plot(a*sqrt(1-(xx/c).^2),xx,'-k','Linewidth',1);
plot([0 AA/sqrt(AA^2+CC^2)],[0 CC/sqrt(AA^2+CC^2)],'-r','Linewidth',2);
plot([0 sin(theta2D)],[0 cos(theta2D)],'-b','Linewidth',2); axis square;
plot([0 sin(theta2D+NormCenter)],[0 cos(theta2D+NormCenter)],'-g','Linewidth',2);

%% Validation on a magnetic single-solution axi case
clear all; clc; close all;
a=1; b=1; c=0.9; Ek=1e-3;
Ro=0.1; Omega_p=0.3; 

Wpx=Ro*cos(pi/4)*sign(Omega_p); Wpy=Ro*sin(pi/4)*sign(Omega_p);
Wpz=Omega_p*cos(asin(Ro./abs(Omega_p)));

[AA,BB,CC]=Flipper(a,b,c,1,Wpx,Wpy,Wpz,-1,0,2,[1 1 1],0.3,[1 1 1]);
[A0,B0,C0]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,0,0,2,[1 1 1],0.3,[1 1 1]);
[A1,B1,C1]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,1,0,2,[1 1 1],0.3,[1 1 1]);
[A2,B2,C2]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,2,0,2,[1 1 1],0.3,[1 1 1]);
[A3,B3,C3]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,3,0,2,[1 1 1],0.3,[1 1 1]);
[A4,B4,C4]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,4,0,2,[1 1 1],0.3,[1 1 1]);
[A5,B5,C5]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,5,0,2,[1 1 1],0.3,[1 1 1]);
[A6,B6,C6]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,6,0,2,[1 1 1],0.3,[1 1 1]);
[A7,B7,C7]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,7,0,2,[1 1 1],0.3,[1 1 1]);
[A8,B8,C8]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,8,0,2,[1 1 1],0.3,[1 1 1]);
[A9,B9,C9]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,9,0,2,[1 1 1],0.3,[1 1 1]);
[A10,B10,C10]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,10,0,2,[1 1 1],0.3,[1 1 1]);
[A11,B11,C11]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,11,0,2,[1 1 1],0.3,[1 1 1]);
[A12,B12,C12]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,12,0,2,[1 1 1],0.3,[1 1 1]);

Compa=[AA,BB,CC; A0,B0,C0; A1 B1 C1; A2 B2 C2;  A3 B3 C3;  A4 B4 C4; A5 B5 C5; A6 B6 C6; A7(end) B7(end) C7(end); ...
    A8(end) B8(end) C8(end); A9(end) B9(end) C9(end); A10(end) B10(end) C10(end); A11(end) B11(end) C11(end); A12(end) B12(end) C12(end)]

Ek=i*Ek;
[jA3,jB3,jC3]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,3,0,2,[1 1 1],0.3,[1 1 1]);
%[jA4,jB4,jC4,jL4]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,4,0,2,[1 1 1],0.3,[1 1 1]); not working 
[jA7,jB7,jC7,jL7]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,7,0,2,[1 1 1],0.3,[1 1 1]);
[jA8,jB8,jC8]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,8,0,2,[1 1 1],0.3,[1 1 1]);
[jA9,jB9,jC9]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,9,0,2,[1 1 1],0.3,[1 1 1]);
[jA10,jB10,jC10,jL10]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,10,0,2,[1 1 1],0.3,[1 1 1]);
[jA11,jB11,jC11,jL11]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,11,0,2,[1 1 1],0.3,[1 1 1]);
[jA12,jB12,jC12]=Flipper(a,b,c,Ek,Wpx,Wpy,Wpz,12,0,2,[1 1 1],0.3,[1 1 1]);

Compa2=[jA3 jB3 jC3; jA7(end) jB7(end) jC7(end); ...
    jA8(end) jB8(end) jC8(end); jA9(end) jB9(end) jC9(end); jA10(end) jB10(end) jC10(end); jA11(end) jB11(end) jC11(end); jA12(end) jB12(end) jC12(end)]

