## FLIPPER: FLows at ImPosed Precession within Ellipsoids in Rotation##
###Overview##
[FLIPPER](https://fr.mathworks.com/matlabcentral/fileexchange/50612-flipper) calculates steady uniform vorticity flows within precessing arbitrary ellipsoids in rotation
###Description##
FLIPPER and its documentation have been released as supplementary data of the following article: C�bron 2015 Fluid Dyn. Res. 47 025504 (doi:10.1088/0169-5983/47/2/025504). Most of the equations solved by FLIPPER are detailed in Busse (1968), Noir (2003), Cebron et al. (2010), and Noir & Cebron (2013). New sets of equations, derived from the previous well-known ones, are also solved by FLIPPER and described in the documentation. Finally, FLIPPER relies on new recasts of equations to perform efficient and accurate solvings of various sets of equations within a unique script (test examples are provided to show how to use FLIPPER).

###Description##
See the documentation in the 'Downloads' section.